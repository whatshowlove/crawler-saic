#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
import json

reload(sys)
sys.setdefaultencoding("utf-8")


def pretty_dict(f, dict_, header=None,  footer=None, separate=1):
    if header:
        print>>f, header
    print>>f, json.dumps(dict_, ensure_ascii=False, indent=1)
    if footer:
        print>>f
    if separate:
        print>>f, '='*15


def print_dict(f, d):
    for i in d:
        print>>f, i, d[i]
    print>>f, '='*15

# 比较字典 a (crawler), b (qi cha cha)
# 判定为不同: key 在 a, b 都存在, 且 not b[key] and a[key] ！= b[key]
def cmp_(da, db):
    for i in da:
        if i in db and db[i] and db[i] != da[i]:
            return False
    return True


# 比较字典 a (qi cha cha), b (crawler)
def rcmp_(da, db):
    for i in da:
        if da[i] and i in db and db[i] != da[i]:
            return False
    return True


# 从 crawler 的字典列表找不存在于 qcc 中的
def ours_diff_qcc(la, lb):
    res = []
    for i in la:
        flag = False
        for j in lb:
            if cmp_(i, j):
                flag = True
                break
        if not flag:
            res.append(i)
    return res


# 从 qcc 的字典列表找不存在于 crawler 中的
def qcc_diff_ours(la, lb):
    res = []
    for i in lb:
        flag = False
        for j in la:
            if rcmp_(i, j):
                flag = True
                break
        if not flag:
            res.append(i)
    return res