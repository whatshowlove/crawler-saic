#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys

reload(sys)
sys.setdefaultencoding("utf-8")


def dict_diff(a, b):
    res = []
    for i in a:
        if i in b and b[i] and a[i] != b[i]:
            res.append((i, a[i], b[i]))
    return res


def check_basic_list(key_, da, db):
    res = dict_diff(da, db)
    path = u'/'.join(['./result', key_, 'basic_list_result'])
    if not res:
        return True
    else:
        with open(path, 'w') as f:
            for i in res:
                print>>f, '[', u'关键词', i[0], ']'
                print>>f, '[ Crawler:  ]\n', i[1]
                print>>f, '[ QiChaCha: ]\n', i[2]
                print>>f, "  =================================  "
        return False