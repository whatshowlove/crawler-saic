#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
from check_util import print_dict, ours_diff_qcc, qcc_diff_ours

reload(sys)
sys.setdefaultencoding("utf-8")


def check_entinvItemList(key_, la, lb):
    assert (isinstance(la, list) and isinstance(lb, list))
    a = ours_diff_qcc(la, lb)
    b = qcc_diff_ours(la, lb)

    if not a and not b:
        return True

    path = u'/'.join(['./result', key_, 'check_entinvItemList'])

    with open(path, 'w') as f:
        # print>>f, 'debug: ', len(la), len(lb)
        # print>>f, 'debug: ', len(a), len(b)
        print>>f, u'crawler 中有 %d 条记录, qcc 有 %d 条记录' % (len(la), len(lb))
        print>>f, u'crawler 中有 %d 条记录与 qcc 中存在差异' % len(a)
        print>>f, '[ From crawler ]'
        for i in a:
            print_dict(f, i)
        print>>f
        print>>f, u'qcc 中有 %d 条记录与 crawler 中存在差异' % len(b)
        print>>f, '[ from qi cha cha ]'
        for i in b:
            print_dict(f, i)

    return False
