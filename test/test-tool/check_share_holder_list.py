#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
from check_util import print_dict, ours_diff_qcc, qcc_diff_ours

reload(sys)
sys.setdefaultencoding("utf-8")


def check_share_holder_list(key_, la, lb):
    a = ours_diff_qcc(la, lb)
    b = qcc_diff_ours(la, lb)

    if not a and not b:
        return True

    path = u'/'.join(['./result', key_, 'share_holder_list_result'])

    with open(path, 'w') as f:
        print>>f, len(a), len(b)
        print>>f, '[ From crawler ]'
        for i in a:
            print_dict(f, i)
        print>>f
        print>>f, '[ From qi cha cha ]'
        for i in b:
            print_dict(f, i)

    return False
