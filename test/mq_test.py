#!/usr/bin/env python
# -*- coding:utf-8 -*-


from stompest.config import StompConfig
from stompest.sync import Stomp
import json
import time

CONFIG = StompConfig('tcp://192.168.31.116:61613')
QUEUE = 'test_rocky'
client = Stomp(CONFIG)
client.connect()
print dir(client)

for i in range(100000):
    print i
    client.send(QUEUE,json.dumps({'test_id':i}))
