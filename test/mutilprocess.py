#!/usr/bin/env python
# -*- coding:utf-8 -*-



from scpy.logger import get_logger
logger = get_logger(__file__)
import multiprocessing

def task(thread_name):

    a_list = zip(range(0,100000),range(1000,200000))
    for a in a_list:
        logger.info('[%s],[%d],[%d]'%(thread_name,a[0],a[1]))
        print a[0],a[1]

pool = multiprocessing.Pool(2)
process_a = multiprocessing.Process(target=task,args=('thread_a',))
process_b = multiprocessing.Process(target=task,args=('thread_b',))
process_a.start()
process_b.start()

#print a_list
#pool.map(task,a_list)

