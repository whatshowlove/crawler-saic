#!/usr/bin/env python
# -*- coding:utf-8 -*-
from kafka import (
    KafkaClient, KeyedProducer,
     RoundRobinPartitioner)

kafka = KafkaClient('192.168.31.101:9092')

# HashedPartitioner is default (currently uses python hash())
producer = KeyedProducer(kafka)
producer.send_messages(b'sc01', b'key1', b'some message')
#producer.send_messages(b'sc01', b'key2', b'this methode')


# Murmur2Partitioner attempts to mirror the java client hashing
#producer = KeyedProducer(kafka, partitioner=Murmur2Partitioner)

# Or just produce round-robin (or just use SimpleProducer)
#producer = KeyedProducer(kafka, partitioner=RoundRobinPartitioner)
