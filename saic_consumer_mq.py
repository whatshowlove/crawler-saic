#!/usr/bin/env python
#-*-coding=utf-8-*-

"""

重庆公司woker
------------
saic_cq_task
    接收参数{'key':工商注册号}

"""

import json

from stompest.config import StompConfig
from stompest.protocol import StompSpec
from stompest.sync import Stomp
from scpy.logger import get_logger

import utils
from config import glob_conf

logger = get_logger(__file__)



from crawler import cq,sc_source,tj,gd,hn,js,nmg,yn,hlj,hen,ah,ln,gx, xj,sd,fj,gs,jl,hb,jx,sx,qh,xz,sax,heb,zj
from crawler.bj_bak import bj

PROVINCE = {'cq':cq,'tj':tj,'sc':sc_source,'gd':gd,'hn':hn,'js':js,'nmg':nmg,
            'yn':yn,'hlj':hlj,'hen':hen,'ah':ah,'ln':ln,'gx':gx,'bj': bj,'xj':xj,'sd':sd,
            'fj':fj, 'gs':gs, 'jl':jl, 'hb':hb, 'jx':jx, 'sx': sx, 'qh': qh, 'xz':xz, 'sax': sax, 'heb': heb,
            'zj':zj}

QUEUE_DICT = {
    'cq':'/queue/saic_cq',
    'tj':'/queue/saic_tj',
    'sc':'/queue/saic_sc',
    'gd':'/queue/saic_gd',
    'hn':'/queue/saic_hn',
    'js':'/queue/saic_js',
    'nmg':'/queue/saic_nmg',
    'yn':'/queue/saic_yn',
    'hlj':'/queue/saic_hlj',
    'hen':'/queue/saic_hen',
    'ah':'/queue/saic_ah',
    'gx':'/queue/saic_gx',
    'bj':'/queue/saic_bj',
    'ln':'/queue/saic_ln',
    'xj':'/queue/saic_xj',
    'jx':'/queue/saic_jx',
    'sd':'/queue/saic_sd',
    'fj':'/queue/saic_fj',
    'gs':'/queue/saic_gs',
    'jl':'/queue/saic_jl',
    'hb':'/queue/saic_hb',
    'sx':'/queue/saic_sx',
    'qh':'/queue/saic_qh',
    'xz':'/queue/saic_xz',
    'sax':'/queue/saic_sax',
    'heb':'/queue/saic_heb',
    'zj':'/queue/saic_zj',
    }

PROVINCE_NAME = ['ah','hlj','hen','cq','sc','tj']

def saic_cq_task(province,queue):

    CONFIG = StompConfig(glob_conf.MQ_HOST)
    client = Stomp(CONFIG)
    client.connect()
    client.subscribe(queue,{StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL,'activemq.prefetchSize':1})
    while True:
        try:
            frame = client.receiveFrame()
            body_dict = json.loads(frame.body)
            key_word = body_dict.get('key')
            #province = body_dict.get('province')
            crawler= PROVINCE.get(province)
            if not crawler:
                logger.info('not value province')
                continue
            print "#"*100
            logger.info('receive %s'%key_word)
            try:
                result = crawler.search2(key_word)
            except Exception,e:
                print "exception"*100
                logger.exception(e)
                utils.save_data((),reg_no=key_word,id_status='exception')
                client.ack(frame)
                continue
            if not result:
                utils.save_data((),reg_no=key_word,id_status='notvalue')
                client.ack(frame)
            else:
                utils.save_data(result,reg_no=key_word,id_status='value')
                client.ack(frame)
        except Exception, e :
            logger.exception(e)



if __name__ == '__main__':
    import sys
    province = sys.argv[1]
    queue = QUEUE_DICT.get(province)
    if not queue:
        logger.info('% queue maped ' %province)
    saic_cq_task(province,queue)

