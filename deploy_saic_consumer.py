from fabric.api import *
import sys

PROJECT = 'saic-crawler-mq'

REMOTE_PATH = '/usr/app/python/saic_consumer_%s'

@parallel(pool_size=10)
def deploy(province, worker_num = 1):
    print province
    with settings(warn_only=True):
        run('rm -rf ' + REMOTE_PATH + "*")
    run('mkdir -p ' + REMOTE_PATH)
    put('*', REMOTE_PATH)
    put('config/ali_glob_conf.py', REMOTE_PATH + '/config/glob_conf.py')
    #with settings(warn_only=True):
    #     run('pkill -f "saic_consumer.py %s"'%province)
    #with cd(REMOTE_PATH):
    #    run('$(nohup python worker_saic.py >& /dev/null < /dev/null &) && sleep 1')
    for i in range(worker_num):
        path = REMOTE_PATH + "_" + str(i)
        run('cp -R ' + 	REMOTE_PATH + ' ' + path)
        with cd(path):
            run('chmod 777 run_worker.sh')
            run('./run_worker.sh %s '%province, pty=False)

@parallel(pool_size=10)
def kill_all(pro):
    with settings(warn_only=True):
        run('pkill -f "saic_consumer.py %s"'%pro)
        run('rm -rf /usr/app/python/saic_consumer_%s*'%pro)

def copy_data():
    run('mkdir -p /usr/app/python/captcha')
    put('/usr/app/python/captcha','/usr/app/python')

if __name__ == "__main__":
    province = sys.argv[1]
    REMOTE_PATH = REMOTE_PATH%province
    env.password = 'qwerfdsa'
    env.hosts = [
        'xu.du@10.168.54.249',
        'xu.du@10.117.52.105',
        'xu.du@10.51.8.148',
        'xu.du@10.117.8.21',
        'xu.du@10.117.209.143',
        'xu.du@10.117.51.186',
        'xu.du@10.117.52.107',
        'xu.du@10.117.209.141',
        'xu.du@10.168.152.171',
        'xu.du@10.117.8.192',

        'xu.du@10.117.192.215',
        'xu.du@10.117.192.177',
        'xu.du@10.117.192.164',
        'xu.du@10.117.192.224',
        'xu.du@10.117.192.173',
        'xu.du@10.117.192.211',
        'xu.du@10.117.192.143',
        'xu.du@10.168.187.113',
        'xu.du@10.174.177.42',
        'xu.du@10.117.192.114',
    ]
    execute(kill_all,province)
    #execute(deploy,province)
    #execute(copy_data)
