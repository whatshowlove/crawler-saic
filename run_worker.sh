province=$1
export TESSDATA_PREFIX=/usr/app/python/captcha
export LD_LIBRARY_PATH=/home//OpenCV-2.3.1/release/lib
export PKG_CONFIG_PATH=/home//OpenCV-2.3.1/release/lib/pkgconfig
alias gcv="g++ `pkg-config --cflags opencv` `pkg-config --libs opencv`"

nohup python saic_consumer.py $province 1>/dev/null 2>/dev/null &
