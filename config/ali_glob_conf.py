#!/usr/bin/env python
#-*-coding=utf-8-*-


PROXY_HOST='10.117.29.191'
MQ_HOST = 'tcp://10.117.29.191:61613'             #mq
CAS_HOST = ['10.252.101.196',]                    #cassandra
CAPTCHA_URL='http://120.55.112.88:44444/captcha' #captcha
MONGO_HOST='10.132.23.104'                        #mongo

QUEUE_INFO_URL = 'http://10.117.29.191:8161/api/jolokia/read/org.apache.activemq:brokerName=localhost,destinationName=%s,destinationType=Queue,type=Broker'

NAME_DB ='organization'
NAME_COLL = 'companyNameStrict_2015_12'
QUEUE_MAX_SIZE = 100000

KAFAKA_HOST='10.168.177.222:9092'
KAFAKA_TOPIC='nacao-topic'
KAFAKA_GROUP = 'nacao-group'

# rb
rb_host = "10.51.29.242"
amqp_url = 'amqp://sc-admin:1qaz2wsx@10.51.29.242:5672/%2F?connection_attempts=3&heartbeat_interval=3600'
rb_user = "sc-admin"
rb_password = "1qaz2wsx"
