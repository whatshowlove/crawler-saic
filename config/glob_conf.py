#!/usr/bin/env python
#-*-coding=utf-8-*-

PROXY_HOST='192.168.31.121'
MQ_HOST = 'tcp://192.168.31.116:61613'
# CAS_HOST = ['192.168.31.48',]
CAS_HOST = ['192.168.31.43',]
CAPTCHA_URL='http://192.168.31.121:44444/captcha'
MONGO_HOST='192.168.31.121'

NAME_DB ='crawler_company_name'
NAME_COLL = 'companyName'

QUEUE_MAX_SIZE = 100000

QUEUE_INFO_URL = 'http://192.168.31.116:8161/api/jolokia/read/org.apache.activemq:brokerName=192.168.31.116,destinationName=%s,destinationType=Queue,type=Broker'

KAFAKA_HOST='192.168.31.114:9092'
KAFAKA_TOPIC='nacao-topic'
KAFAKA_GROUP = 'nacao-group'

# rb
rb_host = "192.168.31.114"
amqp_url = 'amqp://sc-admin:1qaz2wsx@192.168.31.114:5672/%2F?connection_attempts=3&heartbeat_interval=3600'
rb_user = "sc-admin"
rb_password = "1qaz2wsx"
