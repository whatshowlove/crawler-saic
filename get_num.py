#encoding:utf8
import json,time
def complement(num):
    if num == 0:
        return 10
    else:
        return num

def mod(ll,p=10,i=0):
    #print ll
    a1 = int(ll[i])
    s1 = p%11 + a1
    p1 = complement(s1%10)*2
    #print i+1,p%11,a1,p1
    i+=1
    if i == 14:
        p2 = p1%11
        a2 = 11 - p2
        return a2
    return mod(ll,p1,i)

def get_regNo(place,type_num,star=0,end=1000):
    all_dict = json.loads(open('all_regno.json').read())
    head = all_dict[place]
    for i in xrange(star,end):
        num = len(str(i))
        among = (7-num)*'0'+str(i)
        for j in head:
            be14 = str(j)+str(type_num)+str(among)
            #print be14
            rear = mod(be14)
            all_num = be14 + str(rear)
            if len(all_num) == 16:
                continue
            yield all_num

if __name__ == '__main__':
    start = time.time()
    for reg_np in  get_regNo('cq','0',0,100000000) :#参数说明 第一个参数是地方代码，第二个参数是类型号，第三个参数是起始数字，第四个参数是结束数字
        print reg_np
    print time.time() - start
    #for i in its:
    #    print i

