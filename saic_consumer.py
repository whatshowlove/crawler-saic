#!/usr/bin/env python
#-*-coding=utf-8-*-

"""

重庆公司woker
------------
saic_cq_task
    接收参数{'key':工商注册号}

"""

import json

from scpy.logger import get_logger
from scpy.rabbit_asy_consumer import AsyConsumer

import utils
import config.glob_conf

logger = get_logger(__file__)


from crawler import cq,sc,tj,gd,hn,js,nmg,yn,hlj,hen,ah,ln,gx, xj,sd,fj,gs,jl,hb,jx,sx,qh,xz,sax,heb,zj
from crawler.bj_bak import bj

PROVINCE = {'cq':cq,'tj':tj,'sc':sc,'gd':gd,'hn':hn,'js':js,'nmg':nmg,
            'yn':yn,'hlj':hlj,'hen':hen,'ah':ah,'ln':ln,'gx':gx,'bj': bj,'xj':xj,'sd':sd,
            'fj':fj, 'gs':gs, 'jl':jl, 'hb':hb, 'jx':jx, 'sx': sx, 'qh': qh, 'xz':xz, 'sax': sax, 'heb': heb,
            'zj':zj}

QUEUE_DICT = {
    'cq':'/queue/saic_cq',
    'tj':'/queue/saic_tj',
    'sc':'/queue/saic_sc',
    'gd':'/queue/saic_gd',
    'hn':'/queue/saic_hn',
    'js':'/queue/saic_js',
    'nmg':'/queue/saic_nmg',
    'yn':'/queue/saic_yn',
    'hlj':'/queue/saic_hlj',
    'hen':'/queue/saic_hen',
    'ah':'/queue/saic_ah',
    'gx':'/queue/saic_gx',
    'bj':'/queue/saic_bj',
    'ln':'/queue/saic_ln',
    'xj':'/queue/saic_xj',
    'jx':'/queue/saic_jx',
    'sd':'/queue/saic_sd',
    'fj':'/queue/saic_fj',
    'gs':'/queue/saic_gs',
    'jl':'/queue/saic_jl',
    'hb':'/queue/saic_hb',
    'sx':'/queue/saic_sx',
    'qh':'/queue/saic_qh',
    'xz':'/queue/saic_xz',
    'sax':'/queue/saic_sax',
    'heb':'/queue/saic_heb',
    'zj':'/queue/saic_zj',
    }

PROVINCE_NAME = ['ah','hlj','hen','cq','sc','tj']


class MongoConsumer(AsyConsumer):
    def __init__(self, province, amqp_url, queue_name):
        super(MongoConsumer, self).__init__(amqp_url, queue_name=queue_name)
        # AsyConsumer.__init__(self, amqp_url, queue_name=queue_name)
        self.province = province

    def on_message(self, unused_channel, basic_deliver, properties, body):
        try:
            body_dict = json.loads(body)
            key_word = body_dict.get('key')
            crawler = PROVINCE.get(self.province)
            if not crawler:
                logger.info('not value province')
                raise Exception('not value province')

            print "#"*100
            logger.info('receive %s' % key_word)
            try:
                result = crawler.search2(key_word)
            except Exception,e:
                print "exception"*100
                logger.exception(e)
                utils.save_data((), reg_no=key_word, id_status='exception')
                self.acknowledge_message(basic_deliver.delivery_tag)
                return

            if not result:
                utils.save_data((), reg_no=key_word, id_status='notvalue')
                self.acknowledge_message(basic_deliver.delivery_tag)
                return
            else:
                utils.save_data(result, reg_no=key_word, id_status='value')
                self.acknowledge_message(basic_deliver.delivery_tag)
                return
        except Exception, e:
            logger.exception(e)

    def start_consuming(self):
        self._channel.basic_qos(prefetch_count=1)
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(self.on_message,
                                                         self.QUEUE, no_ack=False)

if __name__ == '__main__':
    import sys
    rb_province = "cq"
    # rb_province = sys.argv[1]
    queue = QUEUE_DICT.get(rb_province)
    if not queue:
        logger.info('%s queue mapped ' % rb_province)

    rb_amqp_url = config.glob_conf.amqp_url
    rb_queue_name = QUEUE_DICT.get(rb_province).split('/')[-1]
    # rb_host = config.glob_conf.rb_host
    # rb_count = config.glob_conf.QUEUE_MAX_SIZE

    consumer = MongoConsumer(province=rb_province, amqp_url=rb_amqp_url, queue_name=rb_queue_name)
    print consumer.QUEUE
    try:
        consumer.run()
    except KeyboardInterrupt:
        consumer.stop()



