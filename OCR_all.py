#! /usr/bin/env python  
# -*- coding:utf-8 -*-

"""
ah:安徽
bj,bj1:北京
cq:重庆
fj:福建
gd:广东
gs:甘肃
gx:广西
gz:贵州
hain:海南
hb:湖北
heb:河北
hen:河南
hlj:黑龙江
hn:湖南
jl:吉林
js:江苏
jx:江西
ln:辽宁
nmg:内蒙古
nx:宁夏
qg:全国总局
qh:青海
sax:陕西
sc:四川
sd:山东
sh:上海
sx:山西
tj:天津
xj:新疆
xz:西藏
yn:云南
zj:浙江
zdw:中登网
zx:执行(郑午)
"""
import sys
import os
sys.path.append(__file__[:__file__.rfind('/')] + '/lib')


SPECIAL_OCR_SOURCE = {
    'ah': 'OCR_Chinese',
    'hlj': 'OCR_Chinese',
    'hen': 'OCR_Chinese',
    'gx': 'OCR_Chinese',
    # 'qh': 'OCR_Chinese',
}


def OCR_all(filename, province):
    return getattr(__import__(SPECIAL_OCR_SOURCE.get(province, 'OCR_%s' % province.upper())), 'ocr')(filename)
