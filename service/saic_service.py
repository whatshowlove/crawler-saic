#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys

from scpy.xawesome_location import parse_location

from crawler import (cq, sc,fj,sh,qg,yn,hn,
                     hlj,hen,ah,tj,jx,nmg)

import utils
import qichacha_service

reload(sys)
sys.setdefaultencoding("utf-8")

PROVINCES = {
    u"重庆": cq,
    u"四川": sc,
    u"吉林": "",
    u"辽宁": "",
    u"广东": "",
    u"青海": "",
    u"福建": fj,
    u"上海": sh,
    u"云南": yn,
    u"贵州": "",
    u"天津": tj,
    u"香港": "",
    u"山西": "",
    u"浙江": "",
    u"北京": "",
    u"江西": jx,
    u"河北": "",
    u"台湾": "",
    u"宁夏": "",
    u"山东": "",
    u"河南": hen,
    u"澳门": "",
    u"湖南": hn,
    u"广西": "",
    u"湖北": "",
    u"陕西": "",
    u"江苏": "",
    u"西藏": "",
    u"海南": "",
    u"安徽": ah,
    u"甘肃": "",
    u"新疆": "",
    u"黑龙江": hlj,
    u"内蒙古": nmg,
}

filter_prov = lambda key, places: filter(lambda place: key.startswith(place), places)


def parse_province(key):
    for prov in PROVINCES:
        if prov in key:
            return prov
    info = parse_location(key)
    return info['province'] if info else None


def search_from_db(filter):
    result = utils.query_from_db(utils.COLL_NAME, filter)
    if not (result and isinstance(result, dict)):
        return None
    del result['updateTime']
    return result


def save(data, province, key):
    data['province'] = province
    data['key'] = key
    data['_id'] = data['basicList'][0]['regNo']
    data['companyName'] = data['basicList'][0]['enterpriseName']
    utils.insert_into_db(utils.COLL_NAME, {'_id': data['_id']}, data)


def search(key, province=None, forced_latest=False, strict_mode=False):
    if not province or province not in PROVINCES:
        province = parse_province(key)
    if not province:
        raise Exception(u'没有找到对应的省份')

    if not forced_latest:
        result = search_from_db({'province': province, 'key': key})
        if result:
            return result
        result = search_from_db({'province': province, 'companyName': key})
        if result:
            return result

    if isinstance(PROVINCES[province], basestring):
        raise Exception(u'[%s]还没有完成解析' % province)

    result = PROVINCES[province].search(key)
    if not result or not isinstance(result, dict):
        return None
    save(result, province, key)
    del result['updateTime']
    return result


if __name__ == '__main__':
    result = search(u'湖南华菱钢铁集团有限责任公司')
    print result
