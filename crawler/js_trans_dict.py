#!/usr/bin/env python
# encoding=utf-8
"""
转化字典
"""
import json
from scpy.logger import get_logger
import traceback
import copy

from scpy.xawesome_time import parse_time


# 企业基本信息

basic_dict = {
    'enterpriseName': 'C2',  # 企业名称
    'frName': 'C5',  # 法人姓名
    'regNo': 'C1',  # 工商注册号
    'SocialCreditIdentifier': '',  # 统一社会信用代码
    'regCap': 'C6',  # 注册资金(单位:万元)
    'regCapCur': 'CAPI_TYPE_NAME',  # 注册币种
    'esDate': 'C4',  # 开业日期(YYYY-MM-DD)
    'openFrom': 'C9',  # 经营期限自(YYYY-MM-DD)
    'openTo': '',  # 经营期限至(YYYY-MM-DD)
    'enterpriseType': 'C3',  # 企业(机构)类型
    'enterpriseStatus': 'C13',  # 经营状态(在营、注销、吊销、其他)
    'cancelDate': '',  # 注销日期
    'revokeDate': '',  # 吊销日期
    'address': 'C7',  # 注册地址
    'abuItem': '',  # 许可经营项目
    'cbuItem': '',  # 一般经营项目
    'operateScope': 'C8',  # 经营(业务)范围
    'operateScopeAndForm': '',  # 经营(业务)范围及方式
    'regOrg': 'C11',  # 登记机关
    'ancheYear': '',  # 最后年检年度
    'ancheDate': '',  # 最后年检日期
    'industryPhyCode': '',  # 行业门类代码
    'industryPhyName': '',  # 行业门类名称
    'industryCode': '',  # 国民经济行业代码
    'industryName': '',  # 国民经济行业名称
    'recCap': '',  # 实收资本
    'oriRegNo': '',  # 原注册号
    'auditDate': 'C12',  # 核准日期
}

shareHolder_dict = {
    'shareholderName': 'C2',  # 股东名称
    'shareholderType': 'C1',  # 股东类型
    'country': '',  # 国别
    'subConam': '',  # 认缴出资额(单位:万元)
    'regCapCur': '',  # 币种
    'conDate': '',  # 出资日期
    'fundedRatio': '',  # 出资比例
    # 'funded': 'liacconam',
}

person_dict = {
    "position": "",  # 职务
    "name": "",  # 姓名
    "sex": "",  # 性别
}

# 分支机构
filiation_dict = {
    "brName": "C2",  # 分支机构名称
    "brRegno": "C1",  # 分支机构企业注册号
    "brPrincipal": "",  # 分支机构负责人
    "cbuItem": "",  # 一般经营项目
    "brAddr": "",  # 分支机构地址
    "belong_org": "C3",  # 登记机关
}

# 企业对外投资信息
entinvItem_dict = {
    "entName": "investInfoEntName",  # 企业 (机构 )名称
    "entType": "",  # 企业 (机构 )类型
    "fundedRatio": "",  # 出资比例
    "currency": "",  # 认缴出资币种
    "entStatus": "",  # 企业状态
    "canDate": "",  # 注销日期
    "esDate": "",  # 开业日期
    "regOrg": "",  # 登记机关
    "regCapcur": "",  # 注册资本币种
    "regCap": "",  # 注册资本(万元 )
    "revDate": "",  # 吊销日期
    "name": "",  # 法定代表人姓名
    "subConam": "investInfoConAmount",  # 认缴出资额 (万元)
    "regNo": "investInfoEntRegNo"  # 注册号
}

# 行政处罚
case_dict = {
    'caseTime': '',  # 案发时间
    'caseReason': 'C1',  # 案由
    'caseType': '',  # 案件类型
    'exeSort': 'BREAK_ACTION_TYPE',  # 执行类别
    'caseResult': '',  # 案件结果
    'pendecissDate': 'C5',  # 处罚决定书签发日期
    'penAuth': 'C4',  # 处罚机关
    'illegFact': 'C2',  # 主要违法事实
    'penBasis': '',  # 处罚依据
    'penType': 'DEAL_KIND',  # 处罚种类
    'penResult': 'C3',  # 处罚结果
    'penAm': '',  # 处罚金额
    'penExest': '',  # 处罚执行情况
}

alter_dict = {
    "altDate": "C4",  # 变更日期
    "altItem": "C1",  # 变更事项
    "altBe": "C2",  # 变更前内容
    "altAf": "C3",  # 变更后内容
}

# 异常信息
abnormalOperation_dict = {
    'specauseno': '',  # 序号
    'specause': 'C1',  # 列入经营异常名录原因
    'abntime': 'C2',  # 列入日期
    'recause': 'C3',  # 移出经营异常名录原因
    'retime': 'C4',  # 移出时间
    'decorg': 'C5',  # 做出决定的机关
}

# 抽查检查
checkMessage_dict = {
    'seq_no': '',  # 序号
    'institution': 'C1',  # 检查实施机关
    'check_type': 'C2',  # 类型
    'check_date': 'C3',  # 日期
    'check_result': 'C4',  # 结果
}


# 年报部分
# 企业基本信息
baseInfo_dict = {
    'regNo': 'REG_NO',  # 工商注册号
    'phone': 'TEL',  # 企业联系电话
    'email': 'E_MAIL',  # 电子邮箱
    'zipcode': 'ZIP',  # 邮政编码
    'enterpriseStatus': 'PRODUCE_STATUS',  # 企业经营状态
    'haveWebsite': 'IF_WEBSITE',  # 是否有网站或网店
    'buyEquity': 'IF_INVEST',  # 企业是否有投资信息或购买其他公司股权
    'equityTransfer': 'IF_EQUITY',  # 有限责任公司本年度是否发生股东股权转让
    'address': 'ADDR',  # 住所
    'employeeCount': 'PRAC_PERSON_NUM',  # 从业人数
}


# 网站或网店信息
website_dict = {
    'type': 'WEB_TYPE',  # 类型
    'name': 'WEB_NAME',  # 名称
    'link': 'WEB_URL',  # 网址
}

# 企业对外投资信息
year_entinvItem_dict = {
    "entName": "INVEST_NAME",  # 企业 (机构 )名称
    "entType": "",  # 企业 (机构 )类型
    "fundedRatio": "",  # 出资比例
    "currency": "",  # 认缴出资币种
    "entStatus": "",  # 企业状态
    "canDate": "",  # 注销日期
    "esDate": "",  # 开业日期
    "regOrg": "",  # 登记机关
    "regCapcur": "",  # 注册资本币种
    "regCap": "",  # 注册资本(万元 )
    "revDate": "",  # 吊销日期
    "name": "",  # 法定代表人姓名
    "subConam": "",  # 认缴出资额 (万元)
    "regNo": "INVEST_REG_NO"  # 注册号
}

# 对外提供保证担保信息
# CRED_AMOUNT:"1000.000000万元"
# CRED_NAME:"上海浦东发展银行"
# CRED_TYPE:"合同"
# DEBT_NAME:"江苏沿海森达热电有限公司"
# GUAR_DATE:"2015年02月28日-2015年12月20日"
# GUAR_PERIOD:"期限"
# GUAR_SCOPE:"主债权,利息"
# GUAR_TYPE:"连带保证"
# ID:125999756
# MAIN_ID:125999730
# REG_NO:"320000000012348"
# RN:1

# 发起人及出资信息
investorInformations_dict = {
    'shareholderName': 'STOCK_NAME',  # 发起人,股东
    'subConam': 'STOCK_CAPI',  # 认缴出资额（万元）
    'subConDate': 'SHOULD_CAPI_DATE',  # 认缴出资时间
    'subConType': 'SHOULD_CAPI_TYPE_NAME',  # 认缴出资方式
    'paidConMoney': 'REAL_CAPI',  # 实缴出资额（万元）
    'paidTime': 'REAL_CAPI_DATE',  # 出资时间
    'paidType': 'REAL_CAPI_TYPE_NAME',  # 出资方式
}


# 企业资产状况信息
assetsInfo_dict = {
    'generalAssets': 'NET_AMOUNT',  # 资产总额
    'ownersEequity': 'TOTAL_EQUITY',  # 所有者权益合计
    'revenue': 'SALE_INCOME',  # 营业总收入
    'profit': 'PROFIT_TOTAL',  # 利润总额
    'mainRevenue': 'SERV_FARE_INCOME',  # 营业总收入中主营业务收入
    'netProfit': 'PROFIT_RETA',  # 净利润
    'taxPayment': 'TAX_TOTAL',  # 纳税总额
    'liability': 'DEBT_AMOUNT',  # 负债总额
}

# 股权变更信息
equityChangeInformations_dict = {
    'shareholderName': 'C1',  # 股东
    'equityAfter': 'C3',  # 变更后股权比例
    'equityBefore': 'C2',  # 变更前股权比例
    'time': 'C4'  # 股权变更日期
}

# 修改记录
changeRecords_dict = {
    'changedItem': 'CHANGE_ITEM_NAME',  # 修改事项
    'beforeChange': 'OLD_CONTENT',  # 修改前
    'afterChange': 'NEW_CONTENT',  # 修改后
    'time': 'CHANGE_DATE'  # 修改日期
}

# 时间清洗的key值的,list
time_list = [
    "cancelDate",
    "canDate",
    "retime",
    "revDate",
    "time",
    "revokeDate",
    "openFrom",
    "altDate",
    "subConDate",
    "paidTime",
    "esDate",
    "conDate",
    "check_date",
    "openTo",
    "abntime"
    "auditDate"
]

# 钱清洗的key值的,list
money_list = [
    "netProfit",
    "generalAssets",
    "revenue",
    "regCap",
    "taxPayment",
    "profit",
    "liability",
    "subConam",
    "paidConMoney",
    "ownersEequity",
    "mainRevenue"
]

if __name__ == "__main__":
    pass
    # time_raw_list = [
    #     'esDate',
    #     'openFrom',
    #     'openTo',
    #     'openTo',
    #     'cancelDate',
    #     'revokeDate',
    #     # 'ancheYear',
    #     # 'ancheDate',
    #
    #     'conDate',
    #
    #     'canDate',
    #     'esDate',
    #     'revDate',
    #
    #     'altDate',
    #
    #     'subConDate',
    #     'paidTime',
    #
    #     'time',
    #
    #     'time',
    #
    #     'check_date',
    #
    #     'abntime',
    #     'retime',
    # ]
    # print json.dumps(list(set(time_raw_list)), indent=4, ensure_ascii=False)

    # money_raw_list = [
    #     'regCap',
    #     'subConam',
    #
    #     'regCap',
    #     'subConam',
    #
    #     'subConam',
    #     'paidConMoney',
    #
    #     'generalAssets',
    #     'ownersEequity',
    #     'revenue',
    #     'profit',
    #     'mainRevenue',
    #     'netProfit',
    #     'taxPayment',
    #     'liability',
    #
    # ]

    # print json.dumps(list(set(money_raw_list)), indent=4, ensure_ascii=False)
