# coding

import nmg
import json
from pymongo import MongoClient

client = MongoClient('192.168.31.121')
db = client.crawler_company_name
co = db.companyName

for i in co.find({'province': 'nmg'}).batch_size(5):
    name = i['companyName']
    print json.dumps(nmg.search2(name), ensure_ascii=False, indent=4)
