__author__ = 'huangyu'
import requests
import json
import sys
from scpy.logger import get_logger
import requests
import base64

reload(sys)
sys.setdefaultencoding('utf8')
logger = get_logger(__file__)


def kill_captcha(img, province='zj', img_format='jpg'):
    b64_obj = base64.b64encode(img)
    data = {'source': b64_obj, 'model': 'saic_zj'}
    res = requests.post('http://120.55.112.88:44443/dl', data=data)
    print res.content
    if res.status_code != 200:
        return None
    return res.content


if __name__ == '__main__':
    import os

    raw_img_dir = "/home/huangyu/sc-git-2/zj_num_img/"
    file_list = [raw_img_dir + _ for _ in os.listdir(raw_img_dir)]
    for imagePath in file_list:
        file_obj = open(imagePath).read()
        print kill_captcha(file_obj)
