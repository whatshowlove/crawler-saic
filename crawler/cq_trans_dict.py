#!/usr/bin/env python
# encoding=utf-8
"""
转化字典
"""
import json
from scpy.logger import get_logger
import traceback
import copy

from scpy.xawesome_time import parse_time


# 企业基本信息
basic_dict = {
    'enterpriseName': 'entname',  # 企业名称
    'frName': 'lerep',  # 法人姓名
    'regNo': 'regno',  # 工商注册号
    'regCap': 'regcap',  # 注册资金(单位:万元)
    'regCapCur': 'regcapcur',  # 注册币种
    'esDate': 'estdate',  # 开业日期(YYYY-MM-DD)
    'openFrom': 'opfrom',  # 经营期限自(YYYY-MM-DD)
    'openTo': '',  # 经营期限至(YYYY-MM-DD)
    'enterpriseType': 'enttype',  # 企业(机构)类型
    'enterpriseStatus': 'opstate',  # 经营状态(在营、注销、吊销、其他)
    'cancelDate': '',  # 注销日期
    'revokeDate': 'revdate',  # 吊销日期
    'address': 'dom',  # 注册地址
    'abuItem': '',  # 许可经营项目
    'cbuItem': '',  # 一般经营项目
    'operateScope': 'opscope',  # 经营(业务)范围
    'operateScopeAndForm': 'opscoandform',  # 经营(业务)范围及方式
    'regOrg': 'regorg',  # 登记机关
    'ancheYear': '',  # 最后年检年度
    'ancheDate': 'apprdate',  # 最后年检日期
    'industryPhyCode': '',  # 行业门类代码
    'industryPhyName': '',  # 行业门类名称
    'industryCode': '',  # 国民经济行业代码
    'industryName': '',  # 国民经济行业名称
    'recCap': '',  # 实收资本
    'oriRegNo': '',  # 原注册号
    'auditDate': 'apprdate',  # 核准日期
}

# 股东
shareHolder_dict = {
    'shareholderName': 'inv',  # 股东名称
    'shareholderType': 'invtype',  # 股东类型
    'country': 'certype',  # 国别
    'subConam': 'lisubconam',  # 认缴出资额(单位:万元)
    'regCapCur': '',  # 币种
    'conDate': 'accondate',  # 出资日期
    'fundedRatio': '',  # 出资比例
    # 'funded': 'liacconam',
}

# 主要人员
person_dict = {
    "position": "position",  # 职务
    "name": "name",  # 姓名
    "sex": "",  # 性别
}

# 分支机构
filiation_dict = {
    "brName": "",  # 分支机构名称
    "brRegno": "",  # 分支机构企业注册号
    "brPrincipal": "",  # 分支机构负责人
    "cbuItem": "",  # 一般经营项目
    "brAddr": "",  # 分支机构地址
    "belong_org": "",  # 登记机关
}

# 企业对外投资信息
entinvItem_dict = {
    "entName": "",  # 企业 (机构 )名称
    "entType": "",  # 企业 (机构 )类型
    "fundedRatio": "",  # 出资比例
    "currency": "",  # 认缴出资币种
    "entStatus": "",  # 企业状态
    "canDate": "",  # 注销日期
    "esDate": "",  # 开业日期
    "regOrg": "",  # 登记机关
    "regCapcur": "",  # 注册资本币种
    "regCap": "",  # 注册资本(万元 )
    "revDate": "",  # 吊销日期
    "name": "",  # 法定代表人姓名
    "subConam": "",  # 认缴出资额 (万元)
    "regNo": ""  # 注册号
}

alter_dict = {
    "altDate": "altdate",  # 变更日期
    "altItem": "altitem",  # 变更事项
    "altBe": "altbe",  # 变更前内容
    "altAf": "altaf",  # 变更后内容
}

# 异常信息
abnormalOperation_dict = {
    'specauseno': 'specauseno',  # 序号
    'specause': 'specause',  # 列入经营异常名录原因
    'abntime': 'abntime',  # 列入日期
    'recause': 'remexcpres',  # 移出经营异常名录原因
    'retime': '',  # 移出时间
    'decorg': 'decorg',  # 做出决定的机关
}

# 抽查检查
checkMessage_dict = {
    'seq_no': '',  # 序号
    'institution': 'insauth',  # 检查实施机关
    'check_type': 'instype',  # 类型
    'check_date': 'insdate',  # 日期
    'check_result': 'insresname',  # 结果
}


# 年报部分
# 企业基本信息
baseInfo_dict = {
    'regNo': 'regno',  # 工商注册号
    'phone': 'tel',  # 企业联系电话
    'email': 'email',  # 电子邮箱
    'zipcode': 'postalcode',  # 邮政编码
    'enterpriseStatus': 'opstate',  # 企业经营状态
    'haveWebsite': 'haswebsite',  # 是否有网站或网店
    'buyEquity': 'hasbrothers',  # 企业是否有投资信息或购买其他公司股权
    'equityTransfer': 'istransfer',  # 有限责任公司本年度是否发生股东股权转让
    'address': 'addr',  # 住所
    'employeeCount': 'empnum',  # 从业人数
}


# 网站或网店信息
website_dict = {
    'type': 'webtypename',  # 类型
    'name': 'websitname',  # 名称
    'link': 'domain',  # 网址
}


# 发起人及出资信息
investorInformations_dict = {
    'shareholderName': 'inv',  # 发起人,股东
    'subConam': 'subconam',  # 认缴出资额（万元）
    'subConDate': 'condate',  # 认缴出资时间
    'subConType': 'conform',  # 认缴出资方式
    'paidConMoney': 'acconam',  # 实缴出资额（万元）
    'paidTime': 'accondate',  # 出资时间
    'paidType': 'acconform',  # 出资方式
}


# 企业资产状况信息
assetsInfo_dict = {
    'generalAssets': 'assgro',  # 资产总额
    'ownersEequity': 'totequ',  # 所有者权益合计
    'revenue': 'vendinc',  # 营业总收入
    'profit': 'progro',  # 利润总额
    'mainRevenue': 'maibusinc',  # 营业总收入中主营业务收入
    'netProfit': 'netinc',  # 净利润
    'taxPayment': 'ratgro',  # 纳税总额
    'liability': 'liagro',  # 负债总额
}

# 股权变更信息
equityChangeInformations_dict = {
    'shareholderName': 'stockholder',  # 股东
    'equityAfter': 'stockrightafterbl',  # 变更后股权比例
    'equityBefore': 'stockrightbeforebl',  # 变更前股权比例
    'time': 'stockrightchangedate'  # 股权变更日期
}

# 修改记录
changeRecords_dict = {
    'changedItem': 'modiitem',  # 修改事项
    'beforeChange': 'modibe',  # 修改前
    'afterChange': 'modiaf',  # 修改后
    'time': 'modidate'  # 修改日期
}

# 时间清洗的key值的,list
time_list = [
    "cancelDate",
    "canDate",
    "retime",
    "revDate",
    "time",
    "revokeDate",
    "openFrom",
    "altDate",
    "subConDate",
    "paidTime",
    "esDate",
    "conDate",
    "check_date",
    "openTo",
    "abntime",
    "auditDate",
]

# 钱清洗的key值的,list
money_list = [
    "netProfit",
    "generalAssets",
    "revenue",
    "regCap",
    "taxPayment",
    "profit",
    "liability",
    "subConam",
    "paidConMoney",
    "ownersEequity",
    "mainRevenue",
    "equityBefore",
    "equityAfter",
]

if __name__ == "__main__":
    pass
    # time_raw_list = [
    #     'esDate',
    #     'openFrom',
    #     'openTo',
    #     'openTo',
    #     'cancelDate',
    #     'revokeDate',
    #     # 'ancheYear',
    #     # 'ancheDate',
    #
    #     'conDate',
    #
    #     'canDate',
    #     'esDate',
    #     'revDate',
    #
    #     'altDate',
    #
    #     'subConDate',
    #     'paidTime',
    #
    #     'time',
    #
    #     'time',
    #
    #     'check_date',
    #
    #     'abntime',
    #     'retime',
    # ]
    # print json.dumps(list(set(time_raw_list)), indent=4, ensure_ascii=False)

    # money_raw_list = [
    #     'regCap',
    #     'subConam',
    #
    #     'regCap',
    #     'subConam',
    #
    #     'subConam',
    #     'paidConMoney',
    #
    #     'generalAssets',
    #     'ownersEequity',
    #     'revenue',
    #     'profit',
    #     'mainRevenue',
    #     'netProfit',
    #     'taxPayment',
    #     'liability',
    #
    # ]

    # print json.dumps(list(set(money_raw_list)), indent=4, ensure_ascii=False)
