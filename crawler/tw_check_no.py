# -*- coding:utf8 -*-
# !/usr/bin/env python

import re


def check_no(serial):
    # 共八位，全部為數字型態
    # assert re.findall("^\d{8}$", serial)
    if not re.findall("^\d{8}$", serial):
        return False
    # 各數字分別乘以 1,2,1,2,1,2,4,1
    # 例：統一編號為 53212539
    #
    # Step1 將統編每位數乘以一個固定數字固定值
    #   5   3   2   1   2   5   3   9
    # x 1   2   1   2   1   2   4   1
    # ================================
    #   5   6   2   2   2  10  12   9
    #

    serial_array = map(lambda x: int(x), re.findall("\d", serial))
    num_array = [1, 2, 1, 2, 1, 2, 4, 1]
    serial_array = map(lambda i: serial_array[i] * num_array[i], range(0, 8))

    # Step2 將所得值取出十位數及個位數
    # 十位數 個位數
    #   0      5
    #   0      6
    #   0      2
    #   0      2
    #   0      2
    #   1      0
    #   1      2
    #   0      9
    #
    # 並將十位數與個位數全部結果值加總
    #

    total_sum = reduce(lambda x, y: x + y, map(lambda x: x / 10 + x % 10, serial_array))

    # Step3 判斷結果
    # 第一種:加總值取10的餘數為0
    # 第二種:加總值取9的餘數等於9而且統編的第6碼為7

    if total_sum % 10 == 0 or total_sum % 9 == 9 and serial[5] == 7:
        return True
    else:
        return False


def generate_no():
    for num in xrange(10 ** 7, 10 ** 7 + 10 ** 3):
        a_num = "0" * (8 - len(str(num))) + str(num)
        if not check_no(a_num):
            continue
        else:
            yield a_num


if __name__ == "__main__":
    # _8no = "5312539"
    # _8no = "53212539"
    # _8no = "12222539"
    # print check_no(_8no)
    total_num = 0
    for a in generate_no():
        print a

        total_num += 1

    print total_num
