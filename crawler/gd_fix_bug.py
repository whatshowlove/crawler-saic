#-*- coding:utf8 -*-
#!/usr/bin/env python
from scpy.logger import get_logger

import copy

import requests
import json
import cq


MAXTIME = 10
logger = get_logger(__file__)


def fix_bug(company_name):

    response = requests.get('http://120.26.93.104:7100/api/saic/source?companyName=%s' % company_name)
    # response = requests.get('http://10.51.1.48:7100/api/saic/source?companyName=%s' % company_name)
    raw_data = json.loads(response.content)

    data = json.loads(raw_data["data"])
    base_list = data.get("basicList", [])
    if base_list:
        operateScope = base_list[0].get("operateScope", "")
        if operateScope:
            operateScope = operateScope.replace("<br />", "")
            base_list[0]["operateScope"] = operateScope


            raw_data["data"] = json.dumps(data)
            # print json.dumps(data, ensure_ascii=False, indent=4)
            print json.dumps(json.loads(raw_data["data"]), ensure_ascii=False, indent=4)
            import pdb
            pdb.set_trace()
            # res = requests.post('http://120.26.93.104:7100/api/saic/source', data=json.dumps(raw_data))
            # # res = requests.post('http://10.51.1.48:7100/api/saic/source', data=json.dumps(data))
            # print res.content



if __name__ == "__main__":
    # company_name = '重庆猪八戒威玛科技有限公司'
    # company_name = '中国农业银行股份有限公司重庆武隆支行'
    # company_name = '重庆金易房地产开发（集团）有限公司'
    company_name = '广东恒润华创实业发展有限公司'
    fix_bug(company_name)
    #
    # import pymongo
    # import json
    # clientServer = pymongo.MongoClient('192.168.31.114', 27017)
    # # clientServer = pymongo.MongoClient('10.132.23.104', 27017)
    # db = clientServer.organization
    # collectionServer = db.companyNameStrict
    # reg_no_s = collectionServer.find({'province': 'cq'}).skip(10001).batch_size(5)
    # # reg_no_s = collectionServer.find({'companyName': '重庆金易房地产开发（集团）有限公司'})
    # for reg in reg_no_s:
    #     print '#'*10
    #     print reg
    #     print '#'*10
    #     company_name = reg['companyName']
    #     if u"(" in company_name and u")" in company_name:
    #
    #         # import pdb
    #         # pdb.set_trace()
    #
    #         try:
    #             fix_bug(company_name.replace(u"(", u"（").replace(u")", u"）"))
    #         except Exception, e:
    #             import traceback
    #             traceback.print_exc(e)
    #             # continue
    #             # print reg
