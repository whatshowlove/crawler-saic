import requests
import json
import sys
from scpy.logger import get_logger

reload(sys)
sys.setdefaultencoding('utf8')
logger = get_logger(__file__)
session = requests.session()


def zj_kill_captcha(image, province, img_format):
    if province != "zj" or img_format != "jpg":
        raise Exception("input error!")

    appkey = "97d5256859a76e6b398432c27b8fedb2"
    return request2(appkey, image)


def request2(appkey, image):
    url = "http://op.juhe.cn/vercode/index"
    # img_path = '/home/rocky/Pictures/capt.jpg'
    # image = open(img_path, 'rb')
    params = {
        "key": appkey,
        "codeType": "8001",
        "dtype": "json",
    }

    files = {
        'image': ('image.jpg', image, 'image/jpg',)
    }

    response = session.post(url, params=params, files=files)
    content = response.json()
    res = content
    # print json.dumps(res, ensureascii=False)
    if res:
        error_code = res["error_code"]
        logger.info(res)
        if error_code == 0:
            logger.info(res["result"])
            return res["result"]
        else:
            logger.info("%s:%s" % (res["error_code"], res["reason"]))
            return None
    else:
        logger.info("request api error")
        return None


if __name__ == '__main__':
    pass
    # zj_kill_captcha()
