# -*- coding:utf8 -*-
# !/usr/bin/env python

'''
#全国企业信用信息公示系统（北京）
#维护黄羽
'''

import re
from scpy.logger import get_logger
from table import index, report_index, table_clean, parse_time
import copy
from bj_get_base_info import base_info_run
from bj_get_year_report import year_report_run
import sys

reload(sys)
sys.setdefaultencoding('utf8')

logger = get_logger(__file__)


def extract_share_holder_list(share_holder_table):
    """
    解析股东信息table
    :param share_holder_table:股东信息的网页
    :return:解析后的字典
    """
    share_holder_list = []
    html = re.sub('<tr.*?>', '<tr>', share_holder_table)
    html = html.replace(' ', '').replace('\r', '').replace('\t', '').replace('\n', '')
    detail = re.findall('<tr>.*?</tr>', html)
    if detail and len(detail) > 3:
        for item in detail[2: -1]:
            html = item.replace('<td>', '@@@@@')
            html = re.sub('<td.+?>', '<td>', html)
            html = html.replace('@@@@@', '<td>')
            html = html.replace(' ', '').replace('\r', '').replace('\t', '').replace('\n', '')
            detail_td = re.findall("<td>(.*?)</td>", html)
            if detail_td and len(detail_td) >= 4:
                dic1 = {"shareholderType": detail_td[0], "shareholderName": detail_td[1],
                        "regCapCur": "", "country": "", "fundedRatio": "", "subConam": "", "conDate": ""}
                share_holder_list.append(dic1)
            else:
                raise Exception("添加网页解析方法！")
        return share_holder_list
    else:
        return []


def extract_base(html_res):
    """
    解析工商基本信息
    :param html_res:html源码
    :return:解析后的工商字典
    """
    if not html_res:
        return None

    init_dict = {
        'province': 'bj',
        'basicList': [],
        'shareHolderList': [],
        'personList': [],
        'punishBreakList': [],
        'alidebtList': [],
        'entinvItemList': [],
        'frinvList': [],
        'frPositionList': [],
        'alterList': [],
        'filiationList': [],
        'caseInfoList': [],
        'sharesFrostList': [],
        'sharesImpawnList': [],
        'morDetailList': [],
        'morguaInfoList': [],
        'liquidationList': [],
        'checkMessage': [],
        "abnormalOperation": [],
    }
    res_dict = copy.deepcopy(init_dict)
    res = html_res
    base_table = table_clean(res, "基本信息")
    if base_table:
        basic_list = index("基本信息", base_table)
        res_dict['basicList'] = basic_list

    share_holder_table = table_clean(res, "股东信息")
    if share_holder_table:
        share_holder_list = extract_share_holder_list(share_holder_table)
        res_dict['shareHolderList'] = share_holder_list

    alter_table = table_clean(res, "变更信息")
    if alter_table:
        alter_list = index("变更信息", alter_table)
        res_dict['alterList'] = alter_list

    person_table = table_clean(res, "主要人员信息")
    if person_table:
        person_list = index("主要人员信息", person_table)
        res_dict['personList'] = person_list

    filiation_table = table_clean(res, "分支机构信息")
    if filiation_table:
        filiation_list = index("分支机构信息", filiation_table)
        res_dict['filiationList'] = filiation_list

    liquidation_table = table_clean(res, "清算信息")
    if liquidation_table:
        liquidation_list = index("清算信息", liquidation_table)
        res_dict['liquidationList'] = liquidation_list

    abnormal_operation_table = table_clean(res, "经营异常信息")
    if abnormal_operation_table:
        abnormal_operation_list = index("经营异常信息", abnormal_operation_table)
        for item in abnormal_operation_list:
            recause = item.get('recause', None)
            item['recause'] = re.sub('<.+?>', ' ', recause) if recause else ''
        res_dict['abnormalOperation'] = abnormal_operation_list

    check_message_table = table_clean(res, "抽查检查信息")
    if check_message_table:
        check_message_list = index("抽查检查信息", check_message_table)
        res_dict['checkMessage'] = check_message_list

    return res_dict


def extract_year_report(year_report_html_list):
    """
    解析年报信息
    :param year_report_html_list: 年报源码
    :return: 年报字典
    """
    if not year_report_html_list:
        return None
    year_report_list = []
    init_report = {
        'baseInfo': {},
        'website': {},
        'investorInformations': [],
        'assetsInfo': {},
        'equityChangeInformations': [],
        'changeRecords': [],
    }

    for item in year_report_html_list:
        if not isinstance(item, tuple) or len(item) != 2:
            raise Exception("'year_report_tuple' type error!")

        year = item[0]
        html = item[1]

        report_dict = copy.deepcopy(init_report)

        report_basic_table = table_clean(html, '企业基本信息')
        if report_basic_table:
            report_basic_dict = report_index('企业基本信息', report_basic_table)
            report_dict['baseInfo'] = report_basic_dict

        report_website_table = table_clean(html, '网站或网店信息')
        if report_website_table:
            report_website_dict = report_index('网站或网店信息', report_website_table)
            report_dict['website'] = report_website_dict

        report_assetsInfo_table = table_clean(html, '企业资产状况信息')
        if report_assetsInfo_table:
            report_assetsInfo_dict = report_index('企业资产状况信息', report_assetsInfo_table)
            report_dict['assetsInfo'] = report_assetsInfo_dict

        report_investorInformations_table = table_clean(html, '股东及出资信息')
        if report_investorInformations_table:
            report_investorInformations_list = report_index('股东及出资信息', report_investorInformations_table)
            report_dict['investorInformations'] = report_investorInformations_list

        report_equityChangeInformations_table = table_clean(html, '股权变更信息')
        if report_equityChangeInformations_table:
            report_equityChangeInformations_list = report_index('股权变更信息', report_equityChangeInformations_table)
            report_dict['equityChangeInformations'] = report_equityChangeInformations_list

        report_changeRecords_table = table_clean(html, '修改记录')
        if report_changeRecords_table:
            report_changeRecords_list = report_index('修改记录', report_changeRecords_table)
            report_dict['changeRecords'] = report_changeRecords_list

        report_dict['year'] = year

        year_report_list.append(report_dict)

    return year_report_list


def get_asic(companyName):
    """
    从旧工商网站获取工商html源码,并对进行源码解析
    :param companyName:公司名字或注册号
    :return:None 或者　一个包含html和公司基本信息的tuple
    若公司不存在,返回None;
    若存在,返回一个包含html和公司基本信息的tuple
    """
    html_res = base_info_run(companyName)
    if html_res:
        asic_dict = extract_base(html_res)
        return html_res, asic_dict
    elif html_res is None:
        return None
    else:
        raise Exception("错误！")


def get_year_report(companyName):
    """
    从新工商网站获取公司年报信息源码,并进行解析
    :param companyName:公司名字或注册号
    :return:None 或者　list,
    若公司不存在,返回None;
    若公司存在但年报不存在,返回[];
    若年报存在返回年报,返回的类型list,list里面是一个包含年份和html源码的tuple,即格式为[('2013', 'html'),('2014', 'html')...]
    """
    year_report_html_list = year_report_run(companyName)
    if year_report_html_list and isinstance(year_report_html_list, list):
        year_report_list = extract_year_report(year_report_html_list)
        year_report_raw_html_list = []
        for item in year_report_html_list:
            year_report_raw_html_list.append({item[0]: item[1]})
        return year_report_raw_html_list, year_report_list

    elif year_report_html_list is None:
        return None
    elif year_report_html_list is []:
        return []
    else:
        raise Exception("错误！")


def search2(companyName):
    """
    对工商基本信息和年报信息进行整合
    :param companyName: 公司名字或注册号
    :return:None 或者　一个包含源码字典和工商基本信息字典的tuple
    """
    # raw_html_dict = {}
    asic_tuple = get_asic(companyName)
    if asic_tuple is None:
        return None

    year_report_tuple = get_year_report(companyName)
    if year_report_tuple is None:
        # raise Exception("基本信息和公司年报的公司名字可能不对应！")
        return None  # 公司不存在
    elif year_report_tuple is []:
        year_report_html_list = []  # 公司存在,年报不存在
    elif year_report_tuple and isinstance(year_report_tuple, tuple):
        year_report_html_list = year_report_tuple[0]
    else:
        raise Exception("错误！")

    asic_raw_html = asic_tuple[0]

    raw_html_dict = {
        'province': 'bj',
        'type': '0',
        'html': asic_raw_html,
        'yearList': year_report_html_list,
        'keyword': companyName,
        'companyName': asic_tuple[1]['basicList'][0].get('enterpriseName', ''),
        'json': '',
    }

    res_dict = asic_tuple[1]
    res_dict['yearReportList'] = year_report_tuple[1]

    return raw_html_dict, res_dict


def search(companyName):
    """
    for 实时爬虫
    :param companyName: 公司名字或注册号
    :return:None 或者　一个包含工商基本信息字典
    """
    result = search2(companyName)
    if result is None:
        return None
    elif result and isinstance(result, tuple) and len(result) == 2:
        return result[1]
    else:
        raise Exception("错误!")


if __name__ == "__main__":
    # companyName = '北京百度糯米信息技术有限公司'
    # companyName = '110000450203508'
    # companyName = '北京金顺蝶科技有限公司'
    # companyName = '北京大麦佳居商贸有限责任公司'
    companyName = '北京天下神威科技有限公司'
    # companyName = '110000450203507'
    # res = get_year_report(companyName)
    # res = search(companyName)
    res = search2(companyName)
    import json

    print json.dumps(res, indent=4, ensure_ascii=False)
    # res = extract(companyName)
    # res = search(companyName)

    # print res
