# !/usr/bin/env python
# -*- coding:utf8 -*-

import requests
import base64
# import time
kill_gd_cookie_url = 'http://127.0.0.1:44449'


def gd_kill_cookie(js_str):
    for i in range(30):
        # while True:
        try:
            response = requests.post(kill_gd_cookie_url, data={'data': base64.b64encode(js_str), 'province': 'gd'})
            if response.status_code == 200:
                # print base64.b64decode(response.content)
                return base64.b64decode(response.content)
                # break
            else:
                continue
        except Exception, e:
            print e
            continue


if __name__ == '__main__':
    test_js_str = r"""<script>var x="1383223368@11@__phantomas@1463741240@split@e@location@d@i@dc@document@16@captcha@0@challenge@NQBFvvr7Ii1pRfCwVSZt22FA@return@catch@setTimeout@charAt@DOMContentLoaded@chars@47@cd@May@new@8@charCodeAt@0xFF@t@href@if@JgSe0upZ@addEventListener@1@false@RegExp@GMT@f@cookie@_phantom@x@Expires@l@c@Path@3D@var@20@Fri@0xEDB88320@length@__jsl_clearance@function@189@1500@for@try@join@while@g@rOm9XFMtA3QKV7nYsPGT4lifyWwkq5vcjH2IdxUoCbhERLaz81DNB6@replace@window@else@attachEvent@onreadystatechange".replace(/@*$/,"").split("@"),y="28 24=2e(){30(34.21||34.3){};28 14,a='2d=4.2f|e|';14=(2e(){28 22=[e],9,25,1a='',12='1d%32',1j=2e(25){2h(28 9=e;9<17;9++)25=(25&1f)?(2b^(25>>>1f)):(25>>>1f);h 25};30(1a=22.2j().33(16 1h('\\\\8+','31'),2e(8){h 12.10(8)}).5(',').2j('')+'g%27'){25=-1f;2h(9=e;9<1a.2c;9++)25=(25>>>17)^1j((25^1a.18(9))&19);1c(1===(25^(-1f))>>>e)h 1a;9=e;30(++22[9]===12.2c){22[9++]=e;1c(9===22.2c)22[9]=-1f}}})();a+=14;j('7.1b=7.1b.33(/[\\?|&]d-f/,\\'\\')',2g);b.20=(a+';23=2a, 29-15-c 2:13:29 1i;26=/;');};1c((2e(){2i{h !!34.1e;}i(6){h 1g;}})()){b.1e('11',24,1g);}35{b.36('37',24);}",z=0,f=function(x,y){var a=0,b=0,c=0;x=x.split("");y=y||99;while((a=x.shift())&&(b=a.charCodeAt(0)-77.5))c=(Math.abs(b)<13?(b+48.5):parseInt(a,36))+y*c;return c},g=y.match(/\b\w+\b/g).sort(function(x,y){return f(x)-f(y)}).pop();while(f(g,++z)-x.length){};eval(y.replace(/\b\w+\b/g, function(y){return x[f(y,z)-1]}));</script>"""
    gd_kill_cookie(test_js_str)
