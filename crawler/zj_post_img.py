# -*- coding:utf8 -*-
# !/usr/bin/env python


from scpy.logger import get_logger
import requests
import time

post_url = ""


def post_img(img, answer, result, max_try_time=20):
    while max_try_time > 0:
        max_try_time -= 1
        try:
            requests.post(url=post_url, data={
                "img": img,
                "answer": answer,
                "result": result,
            })
            break
        except Exception, e:
            time.sleep(2)
            continue
    return
