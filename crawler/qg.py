# coding:utf8
'''
#全国企业信用信息公示系统（总局）
#维护肖迪
'''
from scpy.logger import get_logger

logger = get_logger(__file__)
import pycurl
import urllib
import re
from utils import kill_captcha
import StringIO
import random
from bs4 import BeautifulSoup
import json
import table
import requests


def curl(url, data='', cookie='', debug=False):  # 抓取函数[get,post]
    UserAgent = "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)"
    s = StringIO.StringIO()
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.REFERER, 'http://gsxt.scaic.gov.cn/')
    if cookie:
        c.setopt(c.COOKIEJAR, "cookie_file_name4")
    c.setopt(c.COOKIEFILE, "cookie_file_name4")
    c.setopt(pycurl.FOLLOWLOCATION, True)
    if data:
        c.setopt(c.POSTFIELDS, urllib.urlencode(data))
    c.setopt(pycurl.ENCODING, 'gzip')
    c.setopt(c.HTTPHEADER, ['Host:gsxt.saic.gov.cn', 'Upgrade-Insecure-Requests:1',
                            'User-Agent:Googlebot/2.1 (+http://www.googlebot.com/bot.html)',
                            'Origin:http://gsxt.scaic.gov.cn'])
    c.setopt(c.WRITEDATA, s)
    c.perform()
    c.close()
    return s.getvalue()


def verify(name):
    # global detail_url
    loop_num = 0
    while 1:
        loop_num += 1
        try:
            code = curl('http://gsxt.saic.gov.cn/zjgs/search/ent_info_list', cookie=1)
            verify_url = "http://gsxt.saic.gov.cn/zjgs/captcha?preset=&ra=%d" % random.random()
            verify_image = curl(verify_url)
            verify = kill_captcha(verify_image, 'qg', 'jpeg')
            code = re.findall('code:"(.+?)"', code.replace(' ', ''))[0]
            data = {"searchType": 1, "captcha": verify, "session.token": code, "condition.keyword": name}
            search_url = 'http://gsxt.saic.gov.cn/zjgs/search/ent_info_list'
            html = curl(search_url, data)
            try:
                detail_url = re.findall('<a href="(http://gsxt\.saic\.gov\.cn/zjgs/notice/view\?uuid=.+?)"', html)[0]
            except:
                return ''
            detail_html = curl(detail_url)
            return (detail_html, detail_url)
        except:

            if loop_num >= 20:
                logger.info('验证码尝试了20次，退出尝试')
                logger.error('保存word在exception日志:%s' % name)
                logger.exception(name)
                break
            logger.info('验证码错误，正在识别')
            continue


def run(detail_html, **args):
    detail_url = detail_html[1]
    detail_html = detail_html[0]
    yearSource = []
    tables = re.findall('<table[\s\S]+?</table>', detail_html)
    for j in tables:
        word = re.findall('<th colspan="\d+?">(.+?)</th>', j)
        if '股东信息' in j:
            shareHolderList = table.index('股东信息', j)
            if shareHolderList:
                for i in shareHolderList:
                    share_url = re.findall('href="(.+?)"', i['shareHolderdetail'])
                    html = curl(share_url[0]) if share_url else ''
                    html = html.replace(' ', '')
                    subConam = re.findall('invt\.subConAm="(.+?)"', html)[0] if html else ""
                    conDate = ""
                    fundedRatio = ""
                    regCapCur = re.findall('invt\.conForm="(.+?)"', html)[0] if html else ""
                    country = ""
                    i['shareHolderdetail'] = share_url
                    i['subConam'] = subConam
                    i['conDate'] = conDate
                    i['fundedRatio'] = fundedRatio
                    i['regCapCur'] = regCapCur
                    i['country'] = country
        try:
            if '基本信息' == word[0]:
                basicList = table.index(word[0].replace(' ', ''), j)
            # if '股东信息' == word[0]:
            #    shareHolderList = table.index('股东信息',j)
            if '主要人员信息' == word[0]:
                personList = table.index(word[0].replace(' ', ''), j)
            if '变更信息' == word[0]:
                alterList = table.index(word[0].replace(' ', ''), j)
            if '分支机构信息' == word[0]:
                filiationList = table.index(word[0].replace(' ', ''), j)
            if '清算信息' == word[0]:
                liquidationList = table.index(word[0].replace(' ', ''), j)
            if '经营异常' == word[0] or '经营异常信息' == word[0]:
                abnormalOperation = table.index(word[0].replace(' ', ''), j)
        except:
            print word
            continue
    try:
        print basicList
    except:
        basicList = []
    try:
        print shareHolderList
    except:
        shareHolderList = []
    try:
        print personList
    except:
        personList = []
    try:
        print alterList
    except:
        alterList = []
    try:
        print filiationList
    except:
        filiationList = []
    try:
        print liquidationList
    except:
        liquidationList = []
    try:
        print abnormalOperation
    except:
        abnormalOperation = []

    punishBreakList = []
    punishedList = []
    alidebtList = []
    entinvItemList = [
        {"entName": "", "entType": "", "fundedRatio": "", "currency": "", "entStatus": "", "canDate": "", "esDate": "",
         "regOrg": "", "regCapcur": "", "regCap": "", "revDate": "", "name": "", "subConam": "", "regNo": ""}]
    frinvList = []
    frPositionList = []
    caseInfoList = []
    sharesFrostList = []
    sharesImpawnList = []
    morDetailList = []
    morguaInfoList = []
    report_url = detail_url.replace('tab=01', 'tab=02')
    html = curl(report_url)
    report_url = re.findall('"(http://gsxt\.saic\.gov\.cn/zjgs/notice/view_annual.+?)" target="_blank">(\d+)', html)
    yearReportList = []
    yearSource = []
    for i in report_url:
        print i
        year = i[1]
        url = i[0]
        html = curl(url)
        # print html
        table_list = re.findall('<table[\s\S]+?</table>', html)
        for j in table_list:
            if '企业基本信息' in j:
                report_basic = table.report_basic(j)
            if '网站或网店信息' in j:
                report_website = table.report_website(j)
            if '企业资产状况信息' in j:
                report_assetsInfo = table.report_assetsInfo(j)
            if '股东及出资信息' in j:
                report_investorInformations = table.report_investorInformations(j)
            if '股权变更信息' in j:
                report_equityChangeInformations = table.report_equityChangeInformations(j)
            if '修改记录' in j:
                report_changeRecords = table.report_changeRecords(j)
            try:
                print report_basic
            except:
                report_basic = {}
            try:
                print report_website
            except:
                report_website = {}
            try:
                print report_assetsInfo
            except:
                report_assetsInfo = {}
            try:
                print report_investorInformations
            except:
                report_investorInformations = []
            try:
                print report_equityChangeInformations
            except:
                report_equityChangeInformations = []
            try:
                print report_changeRecords
            except:
                report_changeRecords = []
        dit1 = {"year": year, "baseInfo": report_basic, "website": report_website,
                "investorInformations": report_investorInformations, "assetsInfo": report_assetsInfo,
                "equityChangeInformations": report_equityChangeInformations, "changeRecords": report_changeRecords}
        dit_source = {"year": year, "html": html}
        yearSource.append(dit_source)
        yearReportList.append(dit1)
    alldata = {'province': 'qg', "abnormalOperation": abnormalOperation, "basicList": basicList,
               "shareHolderList": shareHolderList, "personList": personList, "punishBreakList": punishBreakList,
               "punishedList": punishedList, "alidebtList": alidebtList, "entinvItemList": entinvItemList,
               "frinvList": frinvList, "frPositionList": frPositionList, "alterList": alterList,
               "filiationList": filiationList, "caseInfoList": caseInfoList, "sharesFrostList": sharesFrostList,
               "sharesImpawnList": sharesImpawnList, "morDetailList": morDetailList, "morguaInfoList": morguaInfoList,
               "liquidationList": liquidationList, "yearReportList": yearReportList}
    if args.get('type') == 1:
        result_source = {"province": "qg", "type": 1, "html": detail_html, "keyword": args.get('searchkey'),
                         "companyName": basicList[0]['enterpriseName'], "yearList": yearSource}
        companyUrl = {"province": "qg", "url": detail_url, "companyName": basicList[0]['enterpriseName'],
                      "method": "get"}
        return (result_source, alldata)
    return alldata


def search(key):
    html = verify(key)
    if html:
        result = run(html)
        return result
    else:
        return {}


def search2(key):
    html = verify(key)
    if html:
        result = run(html, searchkey=key, type=1)
        return result
    else:
        return {}


def search3(data):
    url = data.get('url')
    html = requests.get(url).content
    default = (html, url)
    result = run(default)
    return result


if __name__ == "__main__":
    # print json.dumps(search2(u'中信网络有限公司'), ensure_ascii=False, indent=4)
    print json.dumps(search2(u'中国广核集团有限公司'), ensure_ascii=False, indent=4)
    # print search3('http://gsxt.saic.gov.cn/zjgs/notice/view?uuid=ckWqpmOpcWNSSZdH8pjiNH1.Z9SI040_X.2gYb9CKzg=&tab=01')
