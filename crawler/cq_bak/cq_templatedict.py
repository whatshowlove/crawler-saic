# -*- coding:utf8 -*-

class TemplateDict(object):
    def __init__(self):
        self.testtemplateDict = {
            # '_id': '',
            'companyName': '',
            'province': '',
            'basicList': [  # 企业基本信息
                {
                    'enterpriseName': '',  # 企业名称
                    'frName': '',  # 法人姓名
                    'regNo': '',  # 工商注册号
                    'regCap': '',  # 注册资金(单位:万元)
                    'regCapCur': '',  # 注册币种
                    'esDate': '',  # 开业日期(YYYY-MM-DD)
                    'openFrom': '',  # 经营期限自(YYYY-MM-DD)
                    'openTo': '',  # 经营期限至(YYYY-MM-DD)
                    'enterpriseType': '',  # 企业(机构)类型
                    'enterpriseStatus': '',  # 经营状态(在营、注销、吊销、其他)
                    'cancelDate': '',  # 注销日期
                    'revokeDate': '',  # 吊销日期
                    'address': '',  # 注册地址
                    'abuItem': '',  # 许可经营项目
                    'cbuItem': '',  # 一般经营项目
                    'operateScope': '',  # 经营(业务)范围
                    'operateScopeAndForm': '',  # 经营(业务)范围及方式
                    'regOrg': '',  # 登记机关
                    'ancheYear': '',  # 最后年检年度
                    'ancheDate': '',  # 最后年检日期
                    'industryPhyCode': '',  # 行业门类代码
                    'industryPhyName': '',  # 行业门类名称
                    'industryCode': '',  # 国民经济行业代码
                    'industryName': '',  # 国民经济行业名称
                    'recCap': '',  # 实收资本
                    'oriRegNo': '',  # 原注册号
                    'auditDate': '',  # 核准日期
                },
            ],
            'shareHolderList': [  # shareHolderList
                {
                    'shareholderName': '',  # 股东名称
                    "shareholderType": "",  # 股东类型
                    'country': '',  # 国别
                    'subConam': '',  # 认缴出资额(单位:万元)
                    'regCapCur': '',  # 币种
                    'conDate': '',  # 出资日期
                    'fundedRatio': '',  # 出资比例
                    # 'funded': '',
                    # "gInvAccon":[
                    #     {
                    #         'regCapCur': '',         # 币种
                    #         'conDate': '',           # 出资日期
                    #         'funded': '',       # 出资比例
                    #     },
                    # ],

                },
            ],
            'personList': [  # 企业主要管理人员
                {
                    "position": "",  # 职务
                    "name": "",  # 姓名
                    "sex": ""  # 性别
                },
            ],
            'punishBreakList': [],  # 失信被执行人信息(空)
            'punishedList': [],  # 被执行人信息(空)
            'alidebtList': [],  # 阿里欠贷信息(空)
            'entinvItemList': [  # 企业对外投资信息
                {
                    "entName": "",  # 企业 (机构 )名称
                    "entType": "",  # 企业 (机构 )类型
                    "fundedRatio": "",  # 出资比例
                    "currency": "",  # 认缴出资币种
                    "entStatus": "",  # 企业状态
                    "canDate": "",  # 注销日期
                    "esDate": "",  # 开业日期
                    "regOrg": "",  # 登记机关
                    "regCapcur": "",  # 注册资本币种
                    "regCap": "",  # 注册资本(万元 )
                    "revDate": "",  # 吊销日期
                    "name": "",  # 法定代表人姓名
                    "subConam": "",  # 认缴出资额 (万元)
                    "regNo": ""  # 注册号
                },
            ],
            'frinvList': [],  # 法定代表人对外投资信息(空)
            'frPositionList': [],  # 法定代表人在其他企业任职信息(空)
            'alterList': [  # 企业历史变更信息
                {
                    "altDate": "",  # 变更日期
                    "altItem": "",  # 变更事项
                    "altBe": "",  # 变更前内容
                    "altAf": "",  # 变更后内容
                },
            ],

            'caseInfoList': [],  # 行政处罚(空)


            'filiationList': [],  # 分支机构信息(空)

            'sharesFrostList': [],  # 股权冻结历史信息(空)
            'sharesImpawnList': [],  # 股权出质历史信息(空)
            'morDetailList': [],  # 动产质押信息(空)
            'morguaInfoList': [],  # 动产抵押物信息(空)
            'liquidationList': [],  # 清算信息(空)
            'abnormalOperation': [  # 经营异常信息
                {
                    'specauseno': '',  # 序号
                    'specause': '',  # 列入经营异常名录原因
                    'abntime': '',  # 列入日期
                    'recause': '',  # 移出经营异常名录原因
                    'retime': '',  # 移出时间
                    'decorg': '',  # 做出决定的机关
                }
            ],
            'checkMessage': [  # 抽查检查
                {
                    'seq_no': '',  # 序号
                    'institution': '',  # 检查实施机关
                    'check_type': '',  # 类型
                    'check_date': '',  # 日期
                    'check_result': '',  # 结果
                }
            ],
            'yearReportList': [  # 企业年报
                {
                    'baseInfo': {  # 企业基本信息
                                   'regNo': '',  # 工商注册号
                                   'phone': '',  # 企业联系电话
                                   'email': '',  # 电子邮箱
                                   'zipcode': '',  # 邮政编码
                                   'enterpriseStatus': '',  # 企业经营状态
                                   'haveWebsite': '',  # 是否有网站或网店
                                   'buyEquity': '',  # 企业是否有投资信息或购买其他公司股权
                                   'equityTransfer': '',  # 有限责任公司本年度是否发生股东股权转让
                                   'address': '',  # 住所
                                   'employeeCount': '',  # 从业人数
                                   },
                    'website': {  # 网站或网店信息
                                  'type': '',  # 类型
                                  'name': '',  # 名称
                                  'link': '',  # 网址
                                  },
                    'investorInformations': [  # 发起人及出资信息
                        {
                            'shareholderName': '',  # 发起人,股东
                            # "mNGsentinvsubcon":{#认缴出资
                            #    'subConam': '',  # 认缴出资额（万元）
                            #    'subConDate': '',  # 认缴出资时间
                            #    'subConType': '',  # 认缴出资方式
                            # },
                            #
                            # 'mNGsentinvaccon':{
                            #     'paidConMoney': '',  # 实缴出资额（万元）
                            #     'paidTime': '',  # 出资时间
                            #     'paidType': '',  # 出资方式
                            # },

                            'subConam': '',  # 认缴出资额（万元）
                            'subConDate': '',  # 认缴出资时间
                            'subConType': '',  # 认缴出资方式
                            'paidConMoney': '',  # 实缴出资额（万元）
                            'paidTime': '',  # 出资时间
                            'paidType': '',  # 出资方式

                        },
                    ],  # 发起人及出资信息
                    'assetsInfo': {  # 企业资产状况信息
                                     'generalAssets': '',  # 资产总额
                                     'ownersEequity': '',  # 所有者权益合计
                                     'revenue': '',  # 营业总收入
                                     'profit': '',  # 利润总额
                                     'mainRevenue': '',  # 营业总收入中主营业务收入
                                     'netProfit': '',  # 净利润
                                     'taxPayment': '',  # 纳税总额
                                     'liability': '',  # 负债总额
                                     },
                    'equityChangeInformations': [  # 股权变更信息
                        {
                            'shareholderName': '',  # 股东
                            'equityAfter': '',  # 变更前股权比例
                            'equityBefore': '',  # 变更后股权比例
                            'time': ''  # 股权变更日期
                        },
                    ],  # 股权变更信息
                    'changeRecords': [  # 修改记录
                        {
                            'changedItem': '',  # 修改事项
                            'beforeChange': '',  # 修改前
                            'afterChange': '',  # 修改后
                            'time': ''  # 修改日期
                        },
                    ],
                },
            ],
        }

    def returnTesttemplateDict(self):
        return self.testtemplateDict


if __name__ == "__main__":
    import json

    aTemplateDict = TemplateDict()
    print json.dumps(aTemplateDict.returnTesttemplateDict(), ensure_ascii=True, indent=4)
