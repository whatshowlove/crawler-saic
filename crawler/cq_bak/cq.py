# -*- coding:utf8 -*-
# !/usr/bin/env python
from scpy.logger import get_logger

import cq_getCqCompanyInfo
import cq_templatedict
import cq_transdict
import cq_transformfunction

MAXTIME = 10
logger = get_logger(__file__)


class CqCompanyInfo(object):
    def __init__(self):
        pass

    def cqCompanyInfo(self, key, province):
        '''


        :param key:
        :param province:
        :return:搜索的公司不存在返回空字符串（''），公司存在返回公司信息。如果有多个公司，返回第一个公司
        '''
        if province == 'cq':
            trytime = MAXTIME
            logger.info("Keyword is %s, Province is %s" % (key, province))
            # 获取网络字典
            test_dict = {}
            aSearchCq_class = cq_getCqCompanyInfo.SearchCq()

            while not test_dict and trytime > 0:
                test_dict = aSearchCq_class.getCqCompanyInfo(key)
                if test_dict == None:
                    trytime -= 1
                    logger.error("获取工商信息错误或年报获取错误,重复获取！当前设置次数为%s, 剩余次数为%s", (MAXTIME, trytime))
                    continue
                else:
                    break
            # test_dict = None
            if test_dict == None:
                # return None
                raise Exception, "获取工商信息错误或年报获取错误！"  # 获取错误，不是获取为空
            elif test_dict == '':
                logger.info("公司为空")
                # print "公司为空"
                return ''
            else:
                logger.info("获取原始字典成功！")
                # 载入输出结果的模板字典
                atemplatedict_class = cq_templatedict.TemplateDict()
                template_dict = atemplatedict_class.returnTesttemplateDict()
                # print template_dict

                # 载入key转化字典
                aTransDict_class = cq_transdict.TransDict()
                keyTrans_dict = aTransDict_class.returnTesttransDict()
                # print keyTrans_dict

                # 载入转方法
                formfunction_class = cq_transformfunction.TransDictClass()
                result = formfunction_class.formatDicts(test_dict, template_dict, keyTrans_dict)

                raw_res_dict = {}
                raw_res_dict['json'] = test_dict['IcPublic']
                raw_res_dict['yearList'] = test_dict['allAnnualReports']
                raw_res_dict['type'] = '1'  # type 1,表示json
                raw_res_dict['html'] = ''  # html字段为''
                raw_res_dict['province'] = 'cq'
                raw_res_dict['keyword'] = key  # 搜索条件
                if 'enterpriseName' in result['basicList'][0]:
                    raw_res_dict['companyName'] = result['basicList'][0]['enterpriseName']  # 公司名字
                    # result['companyName'] = result['basicList'][0]['enterpriseName']
                else:
                    raw_res_dict['companyName'] = ''
                    # result['companyName'] = ''

                return raw_res_dict, result
        else:
            pass


CRAWLER = CqCompanyInfo()


def search(key):
    try:
        cqCompany = CRAWLER.cqCompanyInfo(key, 'cq')
        if cqCompany == '':
            logger.info("公司为空")
            return None
        return cqCompany[1]
    except Exception, e:
        import traceback
        traceback.print_exc()
        logger.exception(e)
        raise e


def search2(key):
    try:
        cqCompany = CRAWLER.cqCompanyInfo(key, 'cq')
        if cqCompany == '':
            logger.info("公司为空")
            return None
        return cqCompany
    except Exception, e:
        import traceback
        traceback.print_exc()
        logger.exception(e)
        raise e


if __name__ == "__main__":
    import json
    # aCqCompanyInfo = CqCompanyInfo()
    #
    # 关键字错误
    # dota
    # 重庆
    # 重庆饭店
    # 公司
    #
    # 企业异常
    # 重庆长安工业（集团）有限责任公司长安宾馆
    # 重庆轴承工业公司汽车轴承厂
    # 重庆昇才投资咨询有限公司
    # 长征电器公司重庆公司
    #
    # 企业正常
    # 重庆长安工业（集团）有限责任公司
    # 重庆猪八戒网络有限公司
    #
    # 已吊销
    # 重庆方德信息科技有限公司

    # 重庆力帆摩托车制造有限公司
    # 重庆巴南钟厚玉诊所
    # 重庆掌宝科技
    # 重庆发电厂
    # 重庆大学(重庆)美视电影学院管理有限公司
    # 重庆市酿造调味品公司；重庆酿造调味品总厂
    # 重庆东方轮船公司
    # 旭硕科技(重庆)有限公司
    # 重庆同厦置业顾问有限公司
    # 重庆万仓房地产营销策划有限公司
    # 重庆市合川区唐智生猪养殖场
    # 重庆巴南钟厚玉诊所
    # 奥特斯科技(重庆)有限公司
    # 500000500030401
    # 重庆金易房地产开发（集团）有限公司
    # aresult = aCqCompanyInfo.cqCompanyInfo('dota', 'cq')
    # 500000500030401
    # 重庆市黔江区小莫副食批发部
    # 重庆金易房地产开发（集团）有限公司
    # 重庆市黔江区海雅水蛭养殖场
    # 重庆市黔江区晨耕花椒种植专业合作社
    # 重庆市黔江区荣帆塑钢门窗安装中心
    # 重庆视觉色装饰有限公司南坪分公司
    # 重庆渝开发物资实业公司
    # 重庆渝开发股份有限公司
    # 重庆中天玻璃钢制品有限公司
    # 重庆民本农业发展有限公司
    # 重庆渝宁化肥有限公司

    # print search('50000050003040')
    res = search2('重庆市金牛线缆有限公司歌乐山分公司')
    # print json.dumps(search('重庆市酿造调味品公司'), ensure_ascii=False, indent=4)
    print json.dumps(res)
    print json.dumps(res, ensure_ascii=False, indent=4)
    # print json.dumps(search2('重庆民本农业发展有限公司'), ensure_ascii=False, indent=4)

    # import pdb
    # pdb.set_trace()
