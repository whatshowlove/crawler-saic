# -*- coding:utf8 -*-
class TransDict(object):
    def __init__(self):
        self.testtransDict = {
            # 头
            # '_id':'',                       #公司在数据库ID
            'province': '',  # 省市

            # "pripid": "pripid",             #公司编号
            'regNo': "regno",  # 工商注册号
            'companyName': "entname",  # 企业名称

            # 基本信息
            'basicList': [
                {
                    'enterpriseName': "entname",  # 企业名称
                    'frName': "lerep",  # 法人姓名
                    'regNo': "regno",  # 工商注册号
                    'regCap': "regcap",  # 注册资金(单位:万元)

                    'regCapCur': 'regcapcur',  # 注册币种

                    'esDate': "estdate",  # 开业日期(YYYY-MM-DD)
                    'openFrom': "opfrom",  # 经营期限自(YYYY-MM-DD)

                    'openTo': '',  # 经营期限至(YYYY-MM-DD)

                    'enterpriseType': "enttype",  # 企业(机构)类型
                    'enterpriseStatus': "opstate",  # 经营状态(在营、注销、吊销、其他)

                    'cancelDate': '',  # 注销日期
                    'revokeDate': 'revdate',  # 吊销日期

                    'address': "dom",  # 注册地址
                    'abuItem': "",  # 许可经营项目

                    'cbuItem': '',  # 一般经营项目
                    'operateScope': 'opscope',  # 经营(业务)范围
                    'operateScopeAndForm': 'opscoandform',  # 经营(业务)范围及方式

                    'regOrg': "regorg",  # 登记机关

                    'ancheYear': '',  # 最后年检年度

                    'ancheDate': "apprdate",  # 最后年检日期
                    'industryPhyCode': '',  # 行业门类代码
                    'industryPhyName': '',  # 行业门类名称
                    'industryCode': '',  # 国民经济行业代码
                    'industryName': '',  # 国民经济行业名称
                    'recCap': '',  # 实收资本
                    'oriRegNo': '',  # 原注册号
                    'auditDate': '',  # 核准日期
                }
            ],  # "base",             # 企业基本信息

            # 股份信息
            'shareHolderList': {
                'shareholderName': "inv",  # 股东名称
                'subConam': "lisubconam",  # 认缴出资额(单位:万元)
                'country': "certype",  # 国别
                "shareholderType": "invtype",  # 股东类型
                # gInvsubcon  下面的
                # "gInvAccon":[
                #     {
                #         'regCapCur': "acconform",          #币种
                #         'conDate': "accondate",           # 出资日期
                #
                #         'funded': 'acconam',       # 出资
                #     }
                # ],
                'regCapCur': "",  # 币种
                'conDate': "accondate",  # 出资日期
                # 'funded': 'acconam',       # 出资
                'fundedRatio': '',  # 出资比例
            },  # "investors",

            # 企业主要管理人员
            'personList': [
                {
                    "position": "position",  # 职务
                    "name": "name",  # 姓名
                    "sex": "",  # 性别
                }
            ],  # "members" , # 企业主要管理人员

            'punishBreakList': '',  # 失信被执行人信息(空)
            'punishedList': '',  # 被执行人信息(空)
            'alidebtList': '',  # 阿里欠贷信息(空)

            'entinvItemList': [  # 企业对外投资信息
                {
                    "entName": "",  # 企业 (机构 )名称
                    "entType": "",  # 企业 (机构 )类型
                    "fundedRatio": "",  # 出资比例
                    "currency": "",  # 认缴出资币种
                    "entStatus": "",  # 企业状态
                    "canDate": "",  # 注销日期
                    "esDate": "",  # 开业日期
                    "regOrg": "",  # 登记机关
                    "regCapcur": "",  # 注册资本币种
                    "regCap": "",  # 注册资本(万元 )
                    "revDate": "",  # 吊销日期
                    "name": "",  # 法定代表人姓名
                    "subConam": "",  # 认缴出资额 (万元)
                    "regNo": ""  # 注册号
                },
            ],

            'frinvList': '',  # 法定代表人对外投资信息(空)
            'frPositionList': '',  # 法定代表人在其他企业任职信息(空)

            # 企业历史变更信息
            # "alters" 下面
            'alterList': [
                {
                    "altDate": "altdate",  # 变更日期
                    "altItem": "altitem",  # 变更事项
                    "altBe": "altbe",  # 变更前内容
                    "altAf": "altaf",  # 变更后内容
                }
            ],  # "alters",    # 企业历史变更信息

            'filiationList': "brunchs",  # 分支机构信息(空)

            'caseInfoList': "punishments",  # 行政处罚(空)
            'sharesFrostList': '',  # 股权冻结历史信息(空)
            'sharesImpawnList': 'stock',  # 股权出质历史信息
            'morDetailList': '',  # 动产质押信息(空)

            'morguaInfoList': "motage",  # 动产抵押物信息(空)

            'liquidationList': "accounts",  # 清算信息(空)

            'abnormalOperation': [
                {
                    'specauseno': 'specauseno',  # 序号
                    'specause': 'specause',  # 列入经营异常名录原因
                    'abntime': 'abntime',  # 列入日期
                    'recause': '',  # 移出经营异常名录原因
                    'retime': '',  # 移出时间
                    'decorg': 'decorg',  # 做出决定的机关
                }
            ],

            'checkMessage': [  # 抽查检查
                {
                    'seq_no': '',  # 序号
                    'institution': 'insauth',  # 检查实施机关
                    'check_type': 'instype',  # 类型
                    'check_date': 'insdate',  # 日期
                    'check_result': 'insresname',  # 结果
                }
            ],

            # 企业年报
            # 'yearReportList' 下面
            # "allAnnualReports"
            'yearReportList': [
                {
                    'baseInfo':
                        {
                            'regNo': "regno",  # 工商注册号
                            'phone': "tel",  # 企业联系电话
                            'email': "email",  # 电子邮箱
                            'zipcode': "postalcode",  # 邮政编码
                            'enterpriseStatus': 'opstate',  # 企业经营状态,"opstate":"1",#企业存续状态,0表示否，1表示是
                            'haveWebsite': 'haswebsite',  # 是否有网站或网店,"haswebsite":"1",#是否有网站或网店,0表示否，1表示是
                            'buyEquity': 'hasbrothers',
                        # 企业是否有投资信息或购买其他公司股权,     'buyEquity': '',  # 企业是否有投资信息或购买其他公司股权,
                            'equityTransfer': 'istransfer',
                        # 有限责任公司本年度是否发生股东股权转让,  "istransfer":"1",#有限责任公司本年度是否发生股东股权转让,0表示否，1表示是
                            'address': 'addr',  # 企业通信地址
                            'employeeCount': 'empnum',  # 从业人数, "empnumispublish":"0",#不显示从业人数,0表示否，1表示显示从业人数
                        },
                    # "base",    # 企业基本信息

                    ## 网站或网店信息,部分
                    'website': {
                        'type': "webtypename",  # 类型
                        'name': "websitname",  # 名称
                        'link': "domain",  # 网址
                    },  # "webSites",      # 网站或网店信息

                    # 发起人及出资信息
                    'investorInformations': [
                        {
                            'shareholderName': "inv",  # 发起人#股东
                            # "mNGsentinvsubcon": {
                            #     #"mNGsentinvsubcon"#认缴出资
                            #     'subConam': 'subconam',  # 认缴出资额（万元）
                            #     'subConDate': 'condate',  # 认缴出资时间
                            #     'subConType': 'conform',  # 认缴出资方式
                            # },
                            # "mNGsentinvaccon":{#实缴出资
                            #     #"mNGsentinvaccon"#实缴出资
                            #     'paidConMoney': 'acconam',  # 实缴出资额（万元）
                            #     'paidTime': 'accondate',  # 实缴出资时间
                            #     'paidType': 'acconform',  # 实缴出资方式
                            # },
                            'subConam': 'subconam',  # 认缴出资额（万元）
                            'subConDate': 'condate',  # 认缴出资时间
                            'subConType': 'conform',  # 认缴出资方式
                            'paidConMoney': 'acconam',  # 实缴出资额（万元）
                            'paidTime': 'accondate',  # 出资时间
                            'paidType': 'acconform',  # 出资方式
                        },
                    ],  # "mNGsentinv" ,# 发起人及出资信息#股东及出资信息

                    # 企业资产状况信息
                    # "means"下面
                    # '''
                    # #这里有些字段不确定
                    # '''
                    'assetsInfo': {

                        'generalAssets': 'assgro',  # 资产总额

                        'ownersEequity': '',  # 所有者权益合计

                        'revenue': '',  # 营业总收入

                        'profit': 'progro',  # 利润总额

                        'mainRevenue': 'maibusinc',  # 营业总收入中主营业务收入

                        'netProfit': 'netinc',  # 净利润

                        'taxPayment': 'totequ',  # 纳税总额

                        'liability': 'liagro',  # 负债总额
                    },  # "means",  # 企业资产状况信息

                    # 股权变更信息
                    # "stocks"下面
                    'equityChangeInformations': [
                        {
                            'shareholderName': 'stockholder',  # 股东
                            'equityBefore': 'stockrightbeforebl',  # 变更前股权比例，（单位：百分比）
                            'equityAfter': 'stockrightafterbl',  # 变更后股权比例，（单位：百分比）

                            'time': 'stockrightchangedate',  # 股权变更日期

                        }
                    ],  # "stocks",  # 股权变更信息

                    # 修改记录
                    # "modifies" 下面
                    'changeRecords': [
                        {
                            'changedItem': 'modiitem',  # 修改事项
                            'beforeChange': 'modibe',  # 修改前
                            'afterChange': 'modiaf',  # 修改后
                            'time': 'modidate',  # 修改日期
                        },
                    ],  # "modifies" ,# 修改记录

                },
            ],
        }

    def returnTesttransDict(self):
        return self.testtransDict


if __name__ == "__main__":
    import json

    aTransDict = TransDict()
    print json.dumps(aTransDict.returnTesttransDict(), ensure_ascii=True, indent=4)
