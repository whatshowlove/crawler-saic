#!/usr/bin/env python
# encoding=utf-8
"""
转化字典
"""
import json
from scpy.logger import get_logger
import traceback
import copy

from scpy.xawesome_time import parse_time


# 企业基本信息

basic_dict = {
    'enterpriseName': '',  # 企业名称
    'frName': '',  # 法人姓名
    'regNo': '',  # 工商注册号
    'SocialCreditIdentifier': '',  # 统一社会信用代码
    'regCap': '',  # 注册资金(单位:万元)
    'regCapCur': '',  # 注册币种
    'esDate': '',  # 开业日期(YYYY-MM-DD)
    'openFrom': '',  # 经营期限自(YYYY-MM-DD)
    'openTo': '',  # 经营期限至(YYYY-MM-DD)
    'enterpriseType': '',  # 企业(机构)类型
    'enterpriseStatus': '',  # 经营状态(在营、注销、吊销、其他)
    'cancelDate': '',  # 注销日期
    'revokeDate': '',  # 吊销日期
    'address': '',  # 注册地址
    'abuItem': '',  # 许可经营项目
    'cbuItem': '',  # 一般经营项目
    'operateScope': '',  # 经营(业务)范围
    'operateScopeAndForm': '',  # 经营(业务)范围及方式
    'regOrg': '',  # 登记机关
    'ancheYear': '',  # 最后年检年度
    'ancheDate': '',  # 最后年检日期
    'industryPhyCode': '',  # 行业门类代码
    'industryPhyName': '',  # 行业门类名称
    'industryCode': '',  # 国民经济行业代码
    'industryName': '',  # 国民经济行业名称
    'recCap': '',  # 实收资本
    'oriRegNo': '',  # 原注册号
    'auditDate': '',  # 核准日期
}

shareHolder_dict = {
    'shareholderName': 'inv',  # 股东名称
    'shareholderType': 'invType',  # 股东类型
    'country': '',  # 国别
    'subConam': 'subConAm',  # 认缴出资额(单位:万元)
    'regCapCur': '',  # 币种
    'conDate': 'conDate',  # 出资日期
    'fundedRatio': '',  # 出资比例
    # 'funded': 'liacconam',
}

person_dict = {
    "position": "position",  # 职务
    "name": "name",  # 姓名
    "sex": "",  # 性别
}

# 分支机构
filiation_dict = {
    "seq_no": '',
    "reg_no": '',
    "name": '',
    "belong_org": ''
}

# 企业对外投资信息
entinvItem_dict = {
    "entName": "",  # 企业 (机构 )名称
    "entType": "",  # 企业 (机构 )类型
    "fundedRatio": "",  # 出资比例
    "currency": "",  # 认缴出资币种
    "entStatus": "",  # 企业状态
    "canDate": "",  # 注销日期
    "esDate": "",  # 开业日期
    "regOrg": "",  # 登记机关
    "regCapcur": "",  # 注册资本币种
    "regCap": "",  # 注册资本(万元 )
    "revDate": "",  # 吊销日期
    "name": "",  # 法定代表人姓名
    "subConam": "",  # 认缴出资额 (万元)
    "regNo": ""  # 注册号
}

alter_dict = {
    "altDate": "altDate",  # 变更日期
    "altItem": "altFiledName",  # 变更事项
    "altBe": "altBe",  # 变更前内容
    "altAf": "altAf",  # 变更后内容
}

# 异常信息
abnormalOperation_dict = {
    'specauseno': '',  # 序号
    'specause': '',  # 列入经营异常名录原因
    'abntime': '',  # 列入日期
    'recause': '',  # 移出经营异常名录原因
    'retime': '',  # 移出时间
    'decorg': '',  # 做出决定的机关
}

# 抽查检查
checkMessage_dict = {
    'seq_no': '',  # 序号
    'institution': '',  # 检查实施机关
    'check_type': '',  # 类型
    'check_date': '',  # 日期
    'check_result': '',  # 结果
}


# 年报部分
# 企业基本信息
baseInfo_dict = {
    'regNo': '',  # 工商注册号
    'phone': '',  # 企业联系电话
    'email': '',  # 电子邮箱
    'zipcode': '',  # 邮政编码
    'enterpriseStatus': '',  # 企业经营状态
    'haveWebsite': '',  # 是否有网站或网店
    'buyEquity': '',  # 企业是否有投资信息或购买其他公司股权
    'equityTransfer': '',  # 有限责任公司本年度是否发生股东股权转让
    'address': '',  # 住所
    'employeeCount': '',  # 从业人数
}


# 网站或网店信息
website_dict = {
    'type': '',  # 类型
    'name': '',  # 名称
    'link': '',  # 网址
}


# 发起人及出资信息
investorInformations_dict = {
    'shareholderName': '',  # 发起人,股东
    'subConam': '',  # 认缴出资额（万元）
    'subConDate': '',  # 认缴出资时间
    'subConType': '',  # 认缴出资方式
    'paidConMoney': '',  # 实缴出资额（万元）
    'paidTime': '',  # 出资时间
    'paidType': '',  # 出资方式
}


# 企业资产状况信息
assetsInfo_dict = {
    'generalAssets': '',  # 资产总额
    'ownersEequity': '',  # 所有者权益合计
    'revenue': '',  # 营业总收入
    'profit': '',  # 利润总额
    'mainRevenue': '',  # 营业总收入中主营业务收入
    'netProfit': '',  # 净利润
    'taxPayment': '',  # 纳税总额
    'liability': '',  # 负债总额
}

# 股权变更信息
equityChangeInformations_dict = {
    'shareholderName': '',  # 股东
    'equityAfter': '',  # 变更后股权比例
    'equityBefore': '',  # 变更前股权比例
    'time': ''  # 股权变更日期
}

# 修改记录
changeRecords_dict = {
    'changedItem': '',  # 修改事项
    'beforeChange': '',  # 修改前
    'afterChange': '',  # 修改后
    'time': ''  # 修改日期
}

# 时间清洗的key值的,list
time_list = [
    "cancelDate",
    "canDate",
    "retime",
    "revDate",
    "time",
    "revokeDate",
    "openFrom",
    "altDate",
    "subConDate",
    "paidTime",
    "esDate",
    "conDate",
    "check_date",
    "openTo",
    "abntime",
    "auditDate",
]

# 钱清洗的key值的,list
money_list = [
    "netProfit",
    "generalAssets",
    "revenue",
    "regCap",
    "taxPayment",
    "profit",
    "liability",
    "subConam",
    "paidConMoney",
    "ownersEequity",
    "mainRevenue",
]

if __name__ == "__main__":
    pass
    # time_raw_list = [
    #     'esDate',
    #     'openFrom',
    #     'openTo',
    #     'openTo',
    #     'cancelDate',
    #     'revokeDate',
    #     # 'ancheYear',
    #     # 'ancheDate',
    #
    #     'conDate',
    #
    #     'canDate',
    #     'esDate',
    #     'revDate',
    #
    #     'altDate',
    #
    #     'subConDate',
    #     'paidTime',
    #
    #     'time',
    #
    #     'time',
    #
    #     'check_date',
    #
    #     'abntime',
    #     'retime',
    # ]
    # print json.dumps(list(set(time_raw_list)), indent=4, ensure_ascii=False)

    # money_raw_list = [
    #     'regCap',
    #     'subConam',
    #
    #     'regCap',
    #     'subConam',
    #
    #     'subConam',
    #     'paidConMoney',
    #
    #     'generalAssets',
    #     'ownersEequity',
    #     'revenue',
    #     'profit',
    #     'mainRevenue',
    #     'netProfit',
    #     'taxPayment',
    #     'liability',
    #
    # ]

    # print json.dumps(list(set(money_raw_list)), indent=4, ensure_ascii=False)
