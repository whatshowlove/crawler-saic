import re
from bs4 import BeautifulSoup


def get_page(soup_tag):
    total_page = 0
    page_tag_s = soup_tag.find_all('a')
    page_tag = page_tag_s[-1].get('href') if page_tag_s else ''
    if page_tag:
        total_page_slip = re.findall("javascript:slipFive.*,.*,(\d+),.*", page_tag)
        total_page_go = re.findall("javascript:goPage3.*,(\d+).*", page_tag)
        if total_page_slip:
            total_page = int(total_page_slip[0])
        if total_page_go and int(total_page_go[0]) > 1:
            total_page = int(total_page_go[0])

    return total_page
