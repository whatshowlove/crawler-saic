#!/usr/bin/env python
# -*- coding:utf-8 -*-


class IPException(Exception):
    def __init__(self, message):
        self.message = message
