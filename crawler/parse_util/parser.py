#!/usr/bin/env python
# -*- coding:utf-8 -*-
import json
import sys
from bs4 import BeautifulSoup
import parse_basesic
import parse_alter
import parse_person
# import parse_shareholder
import parse_shares_impawn

from scpy.xawesome_codechecker import timeit

reload(sys)
sys.setdefaultencoding("utf-8")

_build_parser = lambda module: (lambda html: module.parse(BeautifulSoup(html, 'html5lib', from_encoding='utf-8')))

# 解析企业基本信息(basicList)
parse_basic_list = _build_parser(parse_basesic)


# 解析企业历史变更信息(alterList)
parse_alter_list = _build_parser(parse_alter)


# 解析企业主要管理人员(personList)
parse_person_list = _build_parser(parse_person)


# 解析股权出质历史信息(sharesImpawnList)
parse_shares_impawn_list = _build_parser(parse_shares_impawn)

if __name__ == '__main__':
    with open('/home/xlzd/temp/aaa/cq.html', 'r') as f:
        html = f.read()
    print json.dumps(parse_person_list(html), ensure_ascii=False, indent=4)
