#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys

reload(sys)
sys.setdefaultencoding("utf-8")


def parse(soup):
    result = []
    tds = [x.getText().strip() for x in soup.find_all('td')]
    for index, name, position in filter(lambda x: x[1], zip(tds[::3], tds[1::3], tds[2::3])):
        result.append({'name': name, 'position': position, "sex": ''})
    return result


if __name__ == '__main__':
    main()
