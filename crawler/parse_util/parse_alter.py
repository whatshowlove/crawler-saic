#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys
from bs4 import BeautifulSoup
from scpy.xawesome_time import parse_time

reload(sys)
sys.setdefaultencoding("utf-8")


def deal_string(string):
    if string.endswith(u'收起') or string.endswith(u'更多'):
        string = string[:-2].strip()
    return string


def parse_item(tr):
    tds = tr.find_all('td')
    if not tds:
        return None
    length = len(tds)
    if length < 3:
        return None

    item = tds[0].getText().strip()
    if u'变更事项' in item:
        return None
    if length == 4:
        before = deal_string(tds[1].getText().strip())
        after = deal_string(tds[2].getText().strip())
    else:
        before = after = ''
    date = parse_time(tds[-1].getText().strip())
    return {
        'altDate': date,
        'altItem': item,
        'altBe': before,
        'altAf': after,
    }


def parse(soup):
    result = []
    for tr in soup.find_all('tr'):
        try:
            item = parse_item(tr)
            if item:
                result.append(item)
        except Exception, e:
            raise e
    return result


if __name__ == '__main__':
    main()
