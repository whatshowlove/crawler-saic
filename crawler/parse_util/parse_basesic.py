#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""

解析baselist 部分字段
前提：
html 的结构是类似下面这种类型，描述信息和结果在相邻标签
<td>名称</td>
<td>高手有限公司<td>
标签是否为td不影响结果

如果某个字段没取出来，需要在下面的CON_FIG 中配置相应正则
"""

from scpy.date_extractor import extract_first_date
import clearn_util


class BasicList(object):
    def __init__(self):
        """基本信息"""
        self.enterpriseName = '',  # 企业名称
        self.frName = '',  # 法人姓名
        self.regNo = '',  # 工商注册号
        self.regCap = '',  # 注册资金(单位=万元)
        self.regCapCur = '',  # 注册币种
        self.esDate = '',  # 开业日期(YYYY-MM-DD)
        self.openFrom = '',  # 经营期限自(YYYY-MM-DD)
        self.openTo = '',  # 经营期限至(YYYY-MM-DD)
        self.enterpriseType = '',  # 企业(机构)类型
        self.enterpriseStatus = '',  # 经营状态(在营、注销、吊销、其他)
        self.cancelDate = '',  # 注销日期
        self.revokeDate = '',  # 吊销日期
        self.address = '',  # 注册地址
        self.abuItem = '',  # 许可经营项目
        self.cbuItem = '',  # 一般经营项目
        self.operateScope = '',  # 经营(业务)范围
        self.operateScopeAndForm = '',  # 经营(业务)范围及方式
        self.regOrg = '',  # 登记机关
        self.ancheYear = '',  # 最后年检年度
        self.ancheDate = '',  # 最后年检日期
        self.industryPhyCode = '',  # 行业门类代码
        self.industryPhyName = '',  # 行业门类名称
        self.industryCode = '',  # 国民经济行业代码
        self.industryName = '',  # 国民经济行业名称
        self.recCap = '',  # 实收资本
        self.oriRegNo = '',  # 原注册号

    def todict(self):
        return self.__dict__


import re

CON_FIG = {
    'enterpriseName': re.compile(u'名称'),  # 企业名称
    'frName': re.compile(u'法定代表人'),  # 法人姓名
    'regNo': re.compile(u'注册号'),  # 工商注册号
    'regCap': re.compile(u'注册资本'),  # 注册资金(单位:万元)
    'regCapCur': re.compile(u''),  # 注册币种
    'esDate': re.compile(u'成立日期'),  # 开业日期(YYYY-MM-DD)
    'openFrom': re.compile(u'营业期限自|经营期限自'),  # 经营期限自(YYYY-MM-DD)
    'openTo': re.compile(u'营业期限至|经营期限至'),  # 经营期限至(YYYY-MM-DD)
    'enterpriseType': re.compile(u'类型'),  # 企业(机构)类型
    'enterpriseStatus': re.compile(u'登记状态'),  # 经营状态(在营、注销、吊销、其他)
    'cancelDate': re.compile(u''),  # 注销日期
    'revokeDate': re.compile(u''),  # 吊销日期
    'address': re.compile(u'住所|经营场所'),  # 注册地址
    'abuItem': re.compile(u''),  # 许可经营项目
    'cbuItem': re.compile(u''),  # 一般经营项目
    'operateScope': re.compile(u'经营范围'),  # 经营(业务)范围
    'operateScopeAndForm': re.compile(u''),  # 经营(业务)范围及方式
    'regOrg': re.compile(u'登记机关'),  # 登记机关
    'ancheYear': re.compile(u''),  # 最后年检年度
    'ancheDate': re.compile(u'核准日期'),  # 最后年检日期
    'industryPhyCode': re.compile(u''),  # 行业门类代码
    'industryPhyName': re.compile(''),  # 行业门类名称
    'industryCode': re.compile(''),  # 国民经济行业代码
    'industryName': re.compile(''),  # 国民经济行业名称
    'recCap': re.compile(u''),  # 实收资本
    'oriRegNo': re.compile(u''),  # 原注册号
}


def parse_basesic(soup, **kwargs):
    """解析基础信息部分"""
    if kwargs:
        for key, pattern in kwargs.items():
            if key in CON_FIG.keys():
                CON_FIG[key] = pattern
    basic_list = BasicList()
    for k, v in CON_FIG.items():
        cmd_string = 'basic_list.%s=clearn_util.get_nexttag_text(CON_FIG.get("%s"),soup)' % (k, k)
        exec (cmd_string)
        exec ('print "%s" ,basic_list.%s' % (k, k))
    # 格式化时间和数字
    basic_list.esDate = extract_first_date(basic_list.esDate, is_str=True)
    basic_list.openFrom = extract_first_date(basic_list.openFrom, is_str=True)
    basic_list.openTo = extract_first_date(basic_list.openFrom, is_str=True)
    basic_list.ancheDate = extract_first_date(basic_list.ancheDate, is_str=True)
    basic_list.regCap = clearn_util.get_money(basic_list.regCap)
    # print json.dumps(basic_list.todict(),ensure_ascii=False)
    return [basic_list.todict()]


parse = parse_basesic
