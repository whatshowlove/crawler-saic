#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re
import bs4


def get_money(text):
    if not text or not isinstance(text, basestring):
        return
    if not isinstance(text, unicode):
        text = text.decode('utf-8')
    text = re.sub(',', '', text)
    digit_num = re.search(u'\d+.\d+|\d+', text)

    if digit_num:
        return float(digit_num.group())
    return


def strip_all(text):
    """去掉"""
    if not text or not isinstance(text, basestring):
        return ''
    if not isinstance(text, unicode):
        text = unicode(text)
    text = re.sub('[\r\n\t]', '', text).strip()
    return text


def get_next_tag(tag):
    """取下一个tag标签

    """
    next_siblings = list(tag.parent.next_siblings)
    if not next_siblings:
        return None
    next_tags = [tag for tag in next_siblings if isinstance(tag, bs4.Tag)]
    if not next_tags:
        return None
    return next_tags[0]


def get_nexttag_text(pattern, soup):
    """取text满足pattern tag 的下一个tag的text
    pattern :text 要查询的re.pattern 对象
    soup : bs4.BeautifulSoup
    """
    tag = soup.find(text=pattern)
    if not tag:
        return ''
    next_tag = get_next_tag(tag)
    if not next_tag:
        return ''
    return strip_all(next_tag.get_text())
