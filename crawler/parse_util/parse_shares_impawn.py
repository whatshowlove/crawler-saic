#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys
from scpy.xawesome_time import parse_time

reload(sys)
sys.setdefaultencoding("utf-8")


def parse_item(tr):
    tds = [td.getText().strip() for td in tr.find_all('td') if td.parent == tr]
    if not len(tds):
        return None
    return {
        "impoNo": tds[1],  # 登记编号
        "impoPerson": tds[2],  # 出质人
        "impoPersonIdNumber": tds[3],  # 出质人证件号码
        "impAm": tds[4],  # 出质金额
        "impoRg": tds[5],  # 质权人姓名
        "impoRgIdNumber": tds[6],  # 质权人证件号码
        "impoSetupDate": parse_time(tds[7]),  # 股权出质设立登记日期
        "status": tds[8],  # 状态
        "impoRgtype": "",  # 出质人类别
        "imponrecDate": "",  # 出质备案日期
        "impExaeep": "",  # 出质审批部门
        "impSanDate": "",  # 出质批准日期
        "impTo": "",  # 出质截至日期
    }


def parse(soup):
    result = []
    for tr in soup.find_all('tr'):
        try:
            item = parse_item(tr)
            if item:
                result.append(item)
        except Exception, e:
            raise e
    return result


if __name__ == '__main__':
    main()
