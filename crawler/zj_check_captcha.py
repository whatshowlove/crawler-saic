import numpy as np
import Image


def ocr_qualify(filename):
    # im = cv2.imread (filename, 0)
    im = np.array(Image.open(filename).convert('L'), 'f')
    print type(im), im.shape

    # im_size = np.array(im.shape)
    im_seg = im[:, :30]  # im_size[1]*.78

    im_seg[im_seg < 180] = 0
    im_seg[im_seg > 0] = 1

    if sum(sum(im_seg[:])) > 20:
        return False
    else:
        return True
